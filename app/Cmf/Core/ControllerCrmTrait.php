<?php

namespace App\Cmf\Core;

use App\Events\ChangeCacheEvent;
use App\Models\Image;
use App\Services\Image\ImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

trait ControllerCrmTrait
{

    /**
     * Validate mode
     *
     * @param Request $request
     * @param array $rules
     * @param array $attributes
     * @param string|null $method
     * @return mixed
     */
    public function validation(Request $request, $rules, $attributes, $method = null)
    {
        $errors = $this->errors ?? [];

        if (isset($rules[$method])) {
            $rules = $rules[$method];
        }
        return Validator::make($request->all(), $rules, $errors, $attributes);
    }

    /**
     * После любых изменений очистить кэш по этому ключу
     *
     * Если в aKeys user_1.members, то members - это тэг
     *
     * @param array $aKeys
     * @param null|object $oModel
     */
    public function afterChange(array $aKeys = [], $oModel = null): void
    {
        if (method_exists($this, 'thisAfterChange') && !is_null($oModel)) {
            $this->thisAfterChange($oModel);
        } else {
            foreach ($aKeys as $value) {
                $name = strripos($value, '.') ? stristr($value, '.', true) : $value;
                $tag = substr(stristr($value, '.'), 1);
                if ($tag) {
                    event(new ChangeCacheEvent($name, $tag));
                } else {
                    event(new ChangeCacheEvent($name));
                }
            }
        }
    }

    /**
     * Загрузить изображение/файл
     *
     * @param string $class
     * @param int $id
     * @param array $aFiles
     * @param array $options
     * @param string $type
     * @return null|string
     */
    public function upload($class, $id, $aFiles, $options, $type = 'images')
    {
        $result = null;
        switch ($type) {
            case 'images':
                $result = $this->uploadByTypeImages($class, $id, $aFiles, $options);
                break;
        }
        return $result;
    }

    /**
     * Загрузить изображение
     *
     * @param string $class
     * @param int $id
     * @param array $aFiles
     * @param array $options
     * @return null|string
     */
    public function uploadByTypeImages($class, $id, $aFiles, $options)
    {
        $filters = isset($options['filters']) ? $options['filters'] : null;
        $fileName = null;
        try {
            $files = [];
            foreach ($aFiles as $aFile) {
                $oService = new ImageService();
                $files[] = !is_string($aFile) ?
                    $oService->upload($aFile, $options['key'], $id, $filters) :
                    $oService->upload(null, $options['key'], $id, $filters, $aFile, substr(strrchr($aFile, '/'), 1));
            }
            foreach ($files as $key => $file) {
                Image::create([
                    'type' => $options['key'],
                    'imageable_id' => $id,
                    'imageable_type' => $class,
                    'filename' => $file,
                ]);
            }
            if (isset($options['with_main']) && $options['with_main'] && method_exists($this, 'uploadByTypeWithMain')) {
                $this->uploadByTypeWithMain($options['key'], $id);
            }
            $fileName = $files[0];
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
        return $fileName;
    }


    /**
     * Правила для загрузки фотографий, с учетом каждой фотографии
     *
     * @param array $images
     * @param string $rulesKey
     * @param string $validatorKey
     * @param string $validatorAttribute
     * @return array
     */
    public function setRulesForUpload($images, $rulesKey, $validatorKey = 'images', $validatorAttribute = 'image')
    {
        $attributes = $this->getAttributes() ?? [];
        $rules = $this->getRules();

        foreach ($images as $key => $image) {
            $rules[$rulesKey][$validatorKey . '.' . $key] = $rules[$rulesKey][$validatorKey];
            $attributes[$validatorKey . '.' . $key] = $attributes[$validatorAttribute];
        }
        unset($rules[$rulesKey][$validatorKey]);
        return [
            'rules' => $rules,
            'attributes' => $attributes,
        ];
    }

    /**
     * Шаблоны для сущности
     *
     * @return mixed
     */
    protected function setViews()
    {
        return Cache::tags('views')->remember(property_exists($this, 'view') ? $this->view : '', 3600, function () {
            $view = $this->theme . '.content.' . (property_exists($this, 'view') ? $this->view : '');
            return [
                'create' => $this->theme . '.content.default.page.create',
                'show'   => $this->theme . '.content.default.page.show',
                'edit'   => $this->theme . '.content.default.page.edit',

                'index'  => $this->theme . '.content.default.table.index',
                'table'  => $this->theme . '.content.default.table.table',
                'modal'  => $this->theme . '.content.default.modals.container.',

                'customModal' => $this->theme . '.content.components.relationships',
            ];
        });
    }

    /**
     * @param Model $model
     */
    public function beforeDeleteForImages(Model $model)
    {
        $oImages = $model->images;

        if (count($oImages) !== 0) {
            foreach ($oImages as $oImage) {
                if (method_exists($this, 'imageDestroy')) {
                    $request = new Request();
                    $this->imageDestroy($request, $model->id, $oImage->id);
                }
            }
        }
    }

    /**
     * Найти сущность по модели
     *
     * @param string|null $class
     * @param int $id
     * @return mixed
     */
    protected function findByClass($class, $id)
    {
        return $class::find($id);
    }

    /**
     * @param string $model
     * @return mixed
     */
    public static function getControllerByModelName($model)
    {
        $class = '\App\Cmf\Project\\' . Str::studly($model) . '\\' . Str::studly($model) . 'Controller';
        return new $class();
    }

    /**
     * @param $class
     * @return mixed|null
     */
    public static function getModelNameByClass($class)
    {
        return (preg_match('#\\\\([a-zA-Z]+)$#', $class, $match) ? $match[1] : null);
    }

    /**
     * @param string $name
     * @param mixed $data
     */
    public function shareToView($name, $data)
    {
        View::share($name, $data);
    }

    /**
     * @param array $aOrderBy
     */
    public function setOrderBy(array $aOrderBy = []): void
    {
        $this->aOrderBy = $aOrderBy;
    }

    /**
     * @param array $aQuery
     */
    public function setQuery(array $aQuery = []): void
    {
        $this->aQuery = $aQuery;
    }

    /**
     * @param array $image
     */
    public function setImage(array $image = []): void
    {
        $this->image = array_merge($this->image, $image);
    }
}
