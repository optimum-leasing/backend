<?php

namespace App\Cmf\Core\Defaults;

use App\Models\File;
use App\Services\File\FilePathZip;
use App\Services\File\FileService;
use Illuminate\Http\Request;

trait FileableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function fileUpload(Request $request, $id)
    {
        $requestFile = $request->hasFile('file') ? $request->file('file') : $request->get('file');
        $oService = (new FileService());
        $oItem = $this->findByClass($this->class, $id);

        $files = $oItem->files;
        if (!empty($files)) {
            $oService->destroyFiles($oItem);
        }
        $name = $oService->upload($requestFile, self::NAME, $oItem->id);
        File::create([
            'type' => self::NAME,
            'fileable_id' => $oItem->id,
            'fileable_type' => $this->class,
            'filename' => $name,
        ]);
        return responseCommon()->success([]);
    }
}
