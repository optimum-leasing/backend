<?php

namespace App\Cmf\Core\Defaults;

use App\Models\Image;
use App\Services\Image\Facades\ImagePath;
use App\Services\Image\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait ImageableTrait
{
    /**
     * Upload images in modal
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageUpload(Request $request, $id)
    {
        $file = $request->hasFile('images') ? $request->file('images') : $request->get('images');
        $aImages = !is_null($file) ? $file : $request->get('path-images');
        if (empty($aImages)) {
            return responseCommon()->jsonError([
                'error' => ['Изображений не найдено'],
            ]);
        }
        $aRules = $this->setRulesForUpload($aImages, 'upload');
        $rules = $aRules['rules'];
        $attributes = $aRules['attributes'];
        $validation = $this->validation($request, $rules, $attributes, 'upload');
        $checkValidate = $request->get('validate');
        if (($validation->fails() && !$request->exists('path-images') && is_null($checkValidate)) || ($checkValidate)) {
            return responseCommon()->validationMessages($validation);
        }
        $this->upload($this->class ?? 'model', $id, $aImages, $this->image);
        $oItem = $this->findByClass($this->class, $id);
        $returnData = [];
        if ($this->view === 'user') {
            $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
        }
        $returnData['view'] = $this->imageGetView($oItem);
        $this->imageableAfterChangeImage($this->image, $oItem);
        return responseCommon()->success($returnData);
    }

    /**
     * Destroy single image
     *
     * @param Request $request
     * @param int $id
     * @param int $image_id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageDestroy(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);
        if (is_null($oImage)) {
            return responseCommon()->validationMessages(null);
        }
        $oService = new ImageService();
        $oService->deleteImages($oImage->filename, $this->image['key'], $id, $this->image);
        $oImage->destroy($oImage->id);
        $this->setDefaultMainImages($this->image['key'], $id);

        $model = $this->class ?? 'Model';
        $oItem = $model::find($id);
        $returnData = [];
        if (!Auth::guest() && $this->view === 'user' && Auth::user()->id === $oItem->id) {
            $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
        }
        $returnData['view'] = $this->imageGetView($oItem);
        $this->imageableAfterChangeImage($this->image, $oItem);
        return responseCommon()->success($returnData);
    }

    /**
     * Set main single image
     *
     * @param Request $request
     * @param int $id
     * @param int $image_id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageMain(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);
        if (is_null($oImage)) {
            return responseCommon()->validationMessages(null);
        }
        $this->clearMainImages($this->image['key'], $id);
        $oImage = Image::find($image_id);
        $oImage->update([
            'is_main' => 1,
        ]);
        $oItem = $this->findByClass($this->class, $id);
        $returnData = [];
        if ($this->view === 'user' && Auth::user()->id === $oItem->id) {
            $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
        }
        $returnData['view'] = $this->imageGetView($oItem);
        $this->imageableAfterChangeImage($this->image, $oItem);
        return responseCommon()->success($returnData);
    }

    /**
     * @param array $imageSettings
     * @param null|object $oItem
     */
    private function imageableAfterChangeImage(array $imageSettings, $oItem = null): void
    {
        if (isset($imageSettings['clear_cache']) && !is_null($oItem)) {
            $this->afterChange($this->cache, $oItem);
        }
    }

    /**
     * @param object $oItem
     * @return string
     * @throws \Throwable
     */
    private function imageGetView($oItem)
    {
        $col = null;
        $model = null;
        $adminModel = null;
        $form = null;

        $preview = [];
        if (method_exists($this, 'getImageByType')) {
            $preview = $this->getImageByType($oItem);
        }

        return view('cmf.components.gallery.block', [
            'oItem' => $oItem,
            'col' => isset($col) ? $col : 3,
            'model' => isset($model) ? $model : $this->view,
            'adminModel' => isset($adminModel) ? $adminModel : $this->view,
            'form' => isset($form) ? $form : '#' . $this->view . '-form',
            'preview' => $preview,
        ])->render();
    }

    /**
     * @param string $key
     * @param int $id
     */
    private function clearMainImages($key, $id): void
    {
        $oMainImages = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->get();
        foreach ($oMainImages as $oMainImage) {
            $oMainImage->update([
                'is_main' => 0,
            ]);
        }
    }

    /**
     * @param string $key
     * @param int $id
     */
    private function setDefaultMainImages($key, $id): void
    {
        $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->first();
        if (is_null($oMainImage)) {
            $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->first();
            if (!is_null($oMainImage)) {
                $oMainImage->update([
                    'is_main' => 1,
                ]);
            }
        }
    }

    /**
     * @param string $key
     * @param int $id
     */
    protected function uploadByTypeWithMain($key, $id): void
    {
        $oImages = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->get();
        if (empty($oImages[0])) {
            $oImages = Image::where('type', $key)->where('imageable_id', $id)->first();
            $oImages->update([
                'is_main' => 1,
            ]);
        }
    }
}
