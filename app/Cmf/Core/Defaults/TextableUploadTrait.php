<?php

namespace App\Cmf\Core\Defaults;

use App\Models\Image;
use App\Services\Image\Facades\ImagePath;
use App\Services\Image\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait TextableUploadTrait
{
    /**
     * Фильтры для изображений, загруженные с визивига
     * size = 1280 - максимальная ширина
     * @var array
     */
    private $upload = [
        'filters' => [
            'xl' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '1280',
                ],
            ],
        ],
    ];

    /**
     * Тип загрузки в images.type
     * Директория для записи, будет public/news/text/...
     *
     * @var string
     */
    private $uploadType = 'news/text';

    /**
     * @var string
     */
    private $uploadPrefix = 'text';

    /**
     * Загрузка изображения в визивиге
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function textUpload(Request $request, $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        if ($request->hasFile('file')) {
            $url = $this->tmpTextImageUploadCache($request, $oItem);
            return responseCommon()->success([
                'file' => $url,
            ], 'Данные успешно сохранены');
        }
        return responseCommon()->error();
    }

    /**
     * Загрузка файла в tmp директорию и сохранение в кэш
     *
     * @param Request $request
     * @param object $oItem
     * @return string
     */
    private function tmpTextImageUploadCache(Request $request, $oItem)
    {
        $type = $this->uploadType;
        $uid = $request->get('uid');

        $oService = (new ImageService());
        $oService->setTemp();
        $oService->setPrefix($this->uploadPrefix);
        $filename = $oService->upload($request->file('file'), $type, $uid);

        $id = Str::random(16);

        $url = $oService->getAssetToOriginal($type, $uid, $filename);
        if (!Str::startsWith($url, '/')) {
            $url = '/' . $url;
        }
        $this->cacheTextImageMerge($uid, [
            $id => [
                'url' => $url,
                'name' => $filename
            ],
        ]);
        return $url;
    }

    /**
     * Замена текстов в изображениях
     *
     * @param object $oItem
     * @param string $uid
     * @param string $text
     * @return mixed
     */
    public function clearTextImages($oItem, $uid, $text)
    {
        $aData = $this->cacheTextImageGet($uid);

        if (empty($aData) || !$uid) {
            return $text;
        }
        $oService = new ImageService();
        $oService->setPrefix($this->uploadPrefix);
        foreach ($aData as $k => $value) {
            $fileName = $value['name'];
            $fileUrl = $value['url'];
            $filePath = public_path($fileUrl);
            if (File::exists($filePath)) {
                // если это изображение не было удалено из редактора
                // т.е. найдено в тексте
                $pos = strpos($text, $fileUrl);
                if ($pos !== false) {
                    $text = $this->copyFromTmp($oService, $oItem, $filePath, $fileName, $text, $fileUrl);
                }
                /*
                try {

                } catch (\Exception $e) {
                    // ничего пока не исключаем
                    $success = false;
                }
                */
            }
            // обновляем значение в кеше
            unset($aData[$k]);
            $this->cacheTextImageSet($uid, $aData);
        }
        // if $success
        $this->cacheTextImageDelete($uid);
        $this->removeTmpDirectory($uid);
        return $text;
    }

    /**
     * Проверить изображения по тексту, если их нет в тексте, то удалить Image
     *
     * @param object $oItem
     * @param string $text
     */
    protected function checkTextImages($oItem, $text): void
    {
        $type = $this->uploadType;
        $oImages = $oItem->textImages;
        $oService = new ImageService();
        $oService->setPrefix($this->uploadPrefix);
        foreach ($oImages as $oImage) {
            $fileUrl = ImagePath::image($type, array_key_first($this->upload['filters']), $oImage);
            // если изображение не найдено в тексте, то удалить его файлы и из базы
            $pos = strpos($text, $fileUrl);
            if ($pos === false) {
                $oService->deleteImages($oImage->filename, $type, $oItem->id, $this->upload);
                $oImage->destroy($oImage->id);
            }
        }
    }

    /**
     * Удалить все изображения
     *
     * @param object $oItem
     */
    public function removeAllImages($oItem)
    {
        $type = $this->uploadType;
        $oService = new ImageService();
        $oService->setPrefix($this->uploadPrefix);
        $oImages = $oItem->imagesAll;
        foreach ($oImages as $oImage) {
            $oService->deleteImages($oImage->filename, $type, $oItem->id, $this->upload);
            $oImage->destroy($oImage->id);
        }
    }

    /**
     * Удаление временной директории
     *
     * @param string $uid
     */
    public function removeTmpDirectory($uid)
    {
        $type = $this->uploadType;
        $oService = new ImageService();
        $oService->setPrefix($this->uploadPrefix);
        $oService->setTemp();
        $oService->deleteDirectoryByParameters($type, $uid);
    }

    /**
     * Проверить ссылки url
     * Собираем в массив и передаем на загрузку и замену url
     *
     * @param object $oItem
     * @param string $text
     * @return string
     */
    public function clearTextImagesByUrl($oItem, $text): string
    {
        $oService = new ImageService();
        $oService->setPrefix($this->uploadPrefix);

        $positions = strpos_all($text, 'src="http');
        $aUrls = [];
        if (!empty($positions)) {
            foreach ($positions as $position) {
                // обрезаем чтобы получился только url
                $s = mb_substr($text, $position);
                $s = str_replace('src="', '', $s);
                $end = mb_strpos($s, '"');
                $url = mb_substr($s, 0, $end);
                // если такого нет в массиве, чтобы одинаковые не загружались
                // если одинаковая, то достаточно и у нас сделать одинаковую
                if (!in_array($url, $aUrls)) {
                    $aUrls[] = $url;
                }
            }
        }

        foreach ($aUrls as $url) {
            $text = $this->clearTextCopyFromUrl($oService, $url, $oItem, $text);
        }
        return $text;
    }

    /**
     * Проверить ссылки url, если была вставлена ссылка, то скачиваем файл и подставляем другую ссылку
     *
     * @param ImageService $oService
     * @param string $url
     * @param object $oItem
     * @param string $text
     * @return string
     */
    private function clearTextCopyFromUrl(ImageService $oService, $url, $oItem, $text)
    {
        $type = $this->uploadType;

        // новое имя со своим расширением файла
        $aUrl = explode('/', $url);
        $sFileName = $aUrl[array_key_last($aUrl)];
        $fileName = Str::random(12) . '' . $oService->getExtension($sFileName);
        $oService->setTemp();
        $path = $oService->getAssetToOriginal($type, $oItem->id, $fileName);
        $filePath = public_path($path);
        $filePathWithoutFile = str_replace($fileName, '', $filePath);
        if (!File::exists($filePathWithoutFile)) {
            File::makeDirectory($filePathWithoutFile, 0777, true);
        }
        // копировать сначала в storage/tmp
        if (copy($url, $filePath)) {
            // из storage/tmp в актуальную директорию
            $text = $this->copyFromTmp($oService, $oItem, $filePath, $fileName, $text, $url);
        }
        return $text;
    }

    /**
     * Копировать файл из временого хранилища
     * Сохранить файл в базу
     * Удалить файл из временого хранилища
     * Заменить ссылку в тексте на актуальный файл
     *
     * @param ImageService $oService
     * @param object $oItem
     * @param string $filePath
     * @param string $fileName
     * @param string $text
     * @param string $fileUrl
     * @return string
     */
    private function copyFromTmp(ImageService $oService, $oItem, $filePath, $fileName, $text, $fileUrl)
    {
        // копируем файлы с ресайзом
        $newFileName = $this->clearTextCopyFile($oService, $oItem, $filePath, $fileName);
        // сохраняем в базу
        $oImage = $this->tmpTextImageCreateImage($oItem, $newFileName);
        // удаляем временные файлы
        $this->clearTextRemoveTmp($oService, $fileName, $oItem->id);
        //заменяем старые ссылки на изображения
        $text = $this->clearTextReplaceText($text, $oImage, $fileUrl);
        return $text;
    }

    /**
     * @param object $oItem
     * @param string $fileName
     * @return Image
     */
    private function tmpTextImageCreateImage($oItem, $fileName): Image
    {
        $type = $this->uploadType;

        $oImage = Image::create([
            'type' => $type,
            'imageable_id' => $oItem->id,
            'imageable_type' => $this->class,
            'filename' => $fileName,
            'is_main' => 1,
            'status' => 1,
        ]);
        return $oImage;
    }

    /**
     * Скопировать файлы из tmp и скопировать в корректный путь с ресайзом
     *
     * @param ImageService $oService
     * @param object $oItem
     * @param string $filePath
     * @param string $fileName
     * @return string
     */
    private function clearTextCopyFile(ImageService $oService, $oItem, $filePath, $fileName)
    {
        $type = $this->uploadType;
        $oService->setNotTemp();
        $newFileName = $oService->upload($filePath, $type, $oItem->id, $this->upload['filters'], $filePath, $fileName);
        return $newFileName;
    }

    /**
     * Удалить временные файлы в tmp
     *
     * @param ImageService $oService
     * @param string $fileName
     * @param string $uid
     */
    private function clearTextRemoveTmp(ImageService $oService, $fileName, $uid)
    {
        $type = $this->uploadType;
        $oService->setTemp();
        $oService->deleteImages($fileName, $type, $uid);
    }

    /**
     * Заменить в тексте tmp значение на значение по базе
     *
     * @param string $text
     * @param object $oImage
     * @param string $fileUrl
     * @return mixed
     */
    private function clearTextReplaceText($text, $oImage, $fileUrl)
    {
        $type = $this->uploadType;
        $newImageUrl = ImagePath::image($type, array_key_first($this->upload['filters']), $oImage);
        return str_replace($fileUrl, $newImageUrl, $text);
    }


    /**
     * Вытащить из кэша
     *
     * @param string $uid
     * @return mixed
     */
    private function cacheTextImageGet($uid)
    {
        $key = $this->cacheTextImageKey($uid);
        return Cache::get($key);
    }

    /**
     * Занести данные
     *
     * @param string $uid
     * @param array $aData
     */
    private function cacheTextImageSet($uid, $aData = [])
    {
        $key = $this->cacheTextImageKey($uid);
        Cache::put($key, $aData, null);
    }

    /**
     * @param string $uid
     */
    private function cacheTextImageDelete($uid)
    {
        $key = $this->cacheTextImageKey($uid);
        Cache::forget($key);
    }

    /**
     * Смержить данные из кэша
     *
     * @param string $uid
     * @param array $aData
     */
    private function cacheTextImageMerge($uid, $aData = [])
    {
        $aOld = $this->cacheTextImageGet($uid);
        if (is_null($aOld)) {
            $aOld = [];
        }
        $aData = array_merge($aOld, $aData);
        $this->cacheTextImageSet($uid, $aData);
    }

    /**
     * Идентификатор сессии, пример 201906211848-9f3RbwxfJAWBW409
     *
     * @param string $uid
     * @return string
     */
    private function cacheTextImageKey($uid)
    {
        return 'news-wysiwyg:' . $uid;
    }
}
