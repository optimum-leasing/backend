<?php

namespace App\Cmf\Core;

class Helper
{
    /**
     * @var string
     */
    private $as = '';

    /**
     * @var string
     */
    private $url = '';

    /**
     * @var string
     */
    private $prefix = '';

    /**
     * Helper constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->url = config('cmf.url');
        $this->as = config('cmf.as');
        $this->prefix = config('cmf.prefix');

        if ($type === 'cmf') {
            $this->as = $this->as !== '' ? $this->as . '.' : '';
        }
        if ($type === 'auth') {
            $this->as = $this->as !== '' ? $this->as . '.auth.' : 'auth.';
            $this->prefix = $this->prefix !== '' ? $this->prefix . '/auth' : 'auth';
        }
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getAs(): string
    {
        return $this->as;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
