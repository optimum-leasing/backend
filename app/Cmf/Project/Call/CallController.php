<?php

namespace App\Cmf\Project\Call;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Models\Call;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CallController extends MainController
{
    use CallSettingsTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use CallCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Звонки';

    /**
     * Имя сущности
     */
    const NAME = 'call';

    /**
     * Иконка
     */
    const ICON = 'icon-phone';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Call::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'name' => ['required', 'max:255'],
        ],
        'update' => [
            'name' => ['required', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'created_at' => [
            'dataType' => parent::DATA_TYPE_DATE,
            'title' => 'Дата осоздания',
            'in_table' => 1,
            'datetime' => true,
            'seconds' => true,
        ],
        'name' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Имя',
            'in_table' => 2,
        ],
        'email' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Email',
            'in_table' => 3,
        ],
        'phone' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Телефон',
            'in_table' => 1,
        ],
        'description' => [
            'dataType' => parent::DATA_TYPE_TEXTAREA,
            'title' => 'Описание',
        ],
        'status' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Статус',
            'values' => [],
            'in_table' => 4,
        ],
    ];
}
