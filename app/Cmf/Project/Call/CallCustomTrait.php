<?php


namespace App\Cmf\Project\Call;

use App\Models\Call;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

trait CallCustomTrait
{
    protected function thisBeforePaginate()
    {
        View::share('hideSearchBar');
        View::share('hideCreateButton');
    }

    /**
     * @param object|null $oItem
     */
    public function thisPrepareFieldsValues($oItem = null)
    {
        $this->fields['status']['values'] = Call::staticStatuses();
    }
}
