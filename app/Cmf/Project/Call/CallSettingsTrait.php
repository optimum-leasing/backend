<?php

namespace App\Cmf\Project\Call;

trait CallSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => CallController::NAME,
        'title' => CallController::TITLE,
        'description' => null,
        'icon' => CallController::ICON,
    ];

    /**
     * @var array
     */
    public $indexComponents = [
        'search' => false,
        'create' => false,
    ];
}
