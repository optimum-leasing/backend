<?php

namespace App\Cmf\Project;

use App\Http\Controllers\Controller;
use App\Services\Toastr\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class DevController extends Controller
{
    /**
     * @param Request $request
     * @param string $name
     * @return \Illuminate\View\View
     */
    public function php(Request $request, $name): \Illuminate\View\View
    {
        $method = 'php' . Str::title($name);
        return method_exists($this, $method) ? $this->{$method}() : abort(500, 'Method ' . $method . ' not found');
    }

    /**
     * @param Request $request
     * @param string $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function command(Request $request, $name): \Illuminate\Http\RedirectResponse
    {
        $command = $name;

        $this->setCommand($command, $this->commandExists($command));

        return redirect()->back();
    }


    /**
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    private function phpInfo()
    {
        return view('cmf.components.dev.phpinfo');//response()->make('', 200);
    }


    /**
     * @param string $name
     * @return bool
     */
    private function commandExists($name)
    {
        return Arr::has(Artisan::all(), $name);
    }

    /**
     * @param string $name
     * @param bool $exists
     */
    private function setCommand($name, $exists): void
    {
        if ($exists) {
            Artisan::call($name);
            (new Toastr('Команда php artisan ' . $name . ' успешно выполнена.'))->success(false);
        } else {
            (new Toastr('Команда php artisan ' . $name . ' не найдена.'))->error(false);
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function settings(Request $request)
    {
        $view = view('cmf.settings')->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    public function email()
    {

    }

    public function saveSettings()
    {

    }
}
