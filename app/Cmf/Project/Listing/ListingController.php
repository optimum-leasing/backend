<?php

namespace App\Cmf\Project\Listing;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ListingController extends MainController
{
    use ListingSettingsTrait;
    use ListingCustomTrait;
    use ImageableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Листинги';

    /**
     * Имя сущности
     */
    const NAME = 'listing';

    /**
     * Иконка
     */
    const ICON = 'icon-layers';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Listing::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'title' => 'Заголовок',
        'image' => 'Изображение',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'title' => ['required', 'max:255'],
        ],
        'update' => [
            'title' => ['required', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.image' => [
                'title' => 'Галлерея',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Заголовок',
            'in_table' => 1,
            'required' => true,
        ],
        'url' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Ссылка',
            'in_table' => 2,
            'hidden' => true,
        ],
        'description' => [
            'dataType' => parent::DATA_TYPE_TEXTAREA,
            'title' => 'Описание',
        ],
        'prepaid' => [
            'dataType' => parent::DATA_TYPE_NUMBER,
            'title' => 'Аванс (в %)',
            'length' => 2,
        ],
        'price' => [
            'dataType' => parent::DATA_TYPE_NUMBER,
            'title' => 'Стоимость',
            'length' => 7,
        ],
        'price_up' => [
            'dataType' => parent::DATA_TYPE_NUMBER,
            'title' => 'Удорожание (в %)',
            'length' => 2,
        ],
        'contract_term_from' => [
            'dataType' => parent::DATA_TYPE_NUMBER,
            'title' => 'Срок договора лизинга от (месяцев)',
            'length' => 2,
            'group' => 'contract_term',
            'group-col' => 6,
        ],
        'contract_term_to' => [
            'dataType' => parent::DATA_TYPE_NUMBER,
            'title' => 'Срок договора лизинга до (месяцев)',
            'length' => 2,
            'group' => 'contract_term',
            'group-col' => 6,
        ],
        'minimum_package' => [
            'dataType' => parent::DATA_TYPE_TEXTAREA,
            'title' => 'Минимальный пакет документов',
        ],
        'early_redemption' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Досрочный выкуп по истечении',
            'values' => [
                6 => '6 месяцев',
                12 => '12 месяцев',
            ],
        ],
        'has_payments_types' => [
            'dataType' => parent::DATA_TYPE_CHECKBOX,
            'title' => 'Платежи могут быть убывающими, равномерными или сезонными (учитываются особенности ведения бизнеса)',
        ],
        'status' => [
            'dataType' => parent::DATA_TYPE_CHECKBOX,
            'title' => 'Статус',
            'title_form' => 'Активно',
            'in_table' => 5,
            'default' => true,
        ],
    ];
}
