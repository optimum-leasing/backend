<?php

namespace App\Cmf\Project\Page;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PageController extends MainController
{
    use PageSettingsTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use PageCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Страницы';

    /**
     * Имя сущности
     */
    const NAME = 'page';

    /**
     * Иконка
     */
    const ICON = 'icon-doc';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Page::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'name' => ['required', 'max:255'],
        ],
        'update' => [
            'name' => ['required', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.text' => [
                'title' => 'Редактор',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Заголовок',
            'in_table' => 1,
            'required' => true,
        ],
        'url' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Ссылка',
            'in_table' => 2,
            'hidden' => true,
        ],
        'status' => [
            'dataType' => parent::DATA_TYPE_CHECKBOX,
            'title' => 'Статус',
            'title_form' => 'Активно',
            'in_table' => 5,
            'default' => true,
        ],
    ];
}
