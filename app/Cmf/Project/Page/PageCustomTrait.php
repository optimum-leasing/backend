<?php


namespace App\Cmf\Project\Page;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

trait PageCustomTrait
{
    protected function thisBeforePaginate()
    {
        View::share('hideSearchBar');
        View::share('hideCreateButton');
    }
}
