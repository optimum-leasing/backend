<?php

namespace App\Cmf\Project\Page;

trait PageSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => PageController::NAME,
        'title' => PageController::TITLE,
        'description' => null,
        'icon' => PageController::ICON,
    ];

    /**
     * @var array
     */
    public $indexComponents = [
        'search' => false,
        'create' => false,
    ];
}
