<?php

namespace App\Cmf\Project\User;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\MainController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends MainController
{
    use UserSettingsTrait;
    use ImageableTrait;
    use UserCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Пользователи';

    /**
     * Имя сущности
     */
    const NAME = 'user';

    /**
     * Иконка
     */
    const ICON = 'icon-people';

    /**
     * Модель сущности
     */
    public $class = \App\Models\User::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        if (!is_null($model)) {
            array_push($this->rules['update']['email'], Rule::unique('users')->ignore($model->id));
        }
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'unique:users', 'max:255'],
            'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.image' => [
                'title' => 'Галлерея',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'name' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Имя',
            'in_table' => 1,
            'delete_title' => 'пользователя',
            'required' => true,
        ],
        'email' => [
            'dataType' => parent::DATA_TYPE_TEXT,
            'title' => 'Email',
            'required' => true,
        ],
        'status' => [
            'dataType' => parent::DATA_TYPE_CHECKBOX,
            'title' => 'Статус',
            'title_form' => 'Активно',
            'in_table' => 2,
            'default' => true,
        ],
    ];
}
