<?php

namespace App\Cmf\Project\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

trait UserCustomTrait
{
    /**
     * @param Request $request
     */
    public function thisCreate(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $oUser = User::create($data);

        $this->afterChange([], $oUser);
    }

    /**
     * @param Request $request
     * @param object $oUser
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function thisUpdate(Request $request, $oUser)
    {
        $secondValidate = $this->validateByData($request, $oUser);
        if (!$secondValidate['success']) {
            return responseCommon()->jsonError([
                'email' => $secondValidate['message'],
            ]);
        }
        // профиль
        if (isset($request->name) && !is_null($request->name)) {
            $oUser->update($request->all());
        }
        // смена пароля
        if (isset($request->password) && !is_null($request->password)) {
            $old = [
                'email' => $oUser->email,
                'password' => $request->old_password,
            ];
            if (!Auth::attempt($old)) {
                return responseCommon()->jsonError([
                    'old_password' => 'Неверный пароль',
                ]);
            }
            $oUser->update([
                'password' => bcrypt($request->get('password')),
            ]);
        }

        $this->afterChange([], $oUser);

        return responseCommon()->success([
            'name' => $oUser->name,
        ]);
    }

    /**
     * @param object $oUser
     */
    public function thisDestroy($oUser)
    {
        if ($oUser->images) {
            $this->beforeDeleteForImages($oUser);
        }
        $id = $oUser->id;
        $oUser->delete();

        $oItem = new User();
        $oItem->id = $id;
        $this->thisAfterChange($oItem);
    }

    /**
     * @param Request $request
     * @param object $oModel
     * @return array
     */
    protected function validateByData(Request $request, $oModel): array
    {
        $oUsers = $this->class::where('email', $request->get('email'))->get();
        foreach ($oUsers as $oUser) {
            if ($oModel->id !== $oUser->id) {
                return responseCommon()->error([
                    'message' => 'Такое значение поля email уже существует.',
                ]);
            }
        }
        return responseCommon()->success();
    }

    /**
     * @param object $oItem
     */
    public function thisAfterChange($oItem): void
    {
        $this->cache[] = 'user_' . $oItem->id . '.members';
        $this->afterChange($this->cache);
    }

    /**
     * Скрыть/показать сайд бар для адмики, запрос в
     * resources\assets\js\admin\template\app.js
     *
     * @param Request $request
     * @return array
     */
    public function saveSidebarToggle(Request $request): array
    {
        if ($request->exists('toggle') && $request->get('toggle') === 'true') {
            Session::put('sidebar-toggle', $request->get('toggle'));
        } else {
            Session::remove('sidebar-toggle');
        }
        return responseCommon()->success();
    }

    /**
     * Модальное окно для изменений
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function getModalCommand(Request $request): array
    {
        $view = view($this->getCurrentView() . '.components.modals.command', [])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }
}
