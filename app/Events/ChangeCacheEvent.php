<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChangeCacheEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $name;
    public $tag;

    /**
     * ChangeCacheEvent constructor.
     *
     * @param string $name
     * @param string|null $tag
     */
    public function __construct($name, $tag = null)
    {
        $this->name = $name;
        $this->tag = $tag;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
