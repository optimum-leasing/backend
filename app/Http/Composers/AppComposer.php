<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;

class AppComposer
{
    use CommonComposersTrait;

    public function __construct()
    {
        $this->setCommon();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->setCommonCompose($view);
    }
}
