<?php

namespace App\Http\Composers;

use App\Models\Call;
use App\Models\User;
use App\Registries\MemberRegistry;
use App\Services\Image\Facades\ImagePath;
use App\Services\Snapshot;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

class CmfComposer
{
    use CommonComposersTrait;

    private $member = null;
    private $users;
    private $pages;
    private $cmf;
    private $view;
    private $version;
    private $calls;

    public function __construct()
    {
        $this->setCommon();

        $this->member = MemberRegistry::getInstance();
        $this->cmf = $this->remember('cmf', function () {
            return $this->getMenu(app_path('Cmf/Project'));
        });
        if (!is_null(Route::current())) {
            $prefix = config('cmf.as');
            $as = Route::current()->action['as'];

            if ($prefix !== '') {
                $as = str_replace($prefix . '.', '', $as);
            }
            $this->view = stristr($as, '.', true);
        }
        $this->version = $this->remember('version', function () {
            return config('cmf.version');
        });
        $this->calls = $this->remember('calls', function () {
            return Call::active()->get();
        });
        //dd(Auth::user()->image);
        //dd(member()->user->image);
        //dd(member()->canAdminView());
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->setCommonCompose($view);

        $view->with('oComposerMember', $this->member);
        $view->with('oComposerUsers', $this->users);
        $view->with('aComposerPages', $this->pages);
        $view->with('oComposerCalls', $this->calls);
        $view->with('aComposerCmf', $this->cmf);
        $view->with('sComposerRouteView', $this->view);
        $view->with('sComposerVersion', $this->version);
    }
}
