<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Определение если Админский вход
     *
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')
            ->except('logout')
            ->except('logoutAdmin');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->isAdmin = $request->has('adminToken');

        try {
            if (!$request->exists('phpunit')) {
                $request->session()->regenerate();
            }
        } catch (\Exception $e) {
            echo $e;
        }

        $this->clearLoginAttempts($request);

        $response = new JsonResponse();
        $response->setData([
            'success' => true,
            'redirect' => $request->get('backTo') ?? redirect()->back()->getTargetUrl(),
        ]);

        return $this->authenticated($request, $this->guard()->user()) ?: $response;
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showAdminLoginForm()
    {
        return view('cmf.auth.login');
    }

    public function showActivateForm()
    {
        return view('cmf.auth.login');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function logoutAdmin(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: responseCommon()->success([
            'redirect' => url('/'),
        ]);
    }
}
