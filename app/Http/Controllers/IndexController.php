<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\Page;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Fomvasss\LaravelMetaTags\Facade as MetaTag;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('app.index');
    }

    public function refer(Request $request)
    {
        $oRefer = \App\Models\Refer::create([
            'form_id' => $request->get('form_id'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'level' => $request->get('level'),
            'ip' => $request->ip(),
            'utm' => json_encode($request->all()),
            'phone' => preg_replace('/[^0-9]/', '', $request->get('phone')),
        ]);

        $oManagers = User::role(User::ROLE_MANAGER_A)->get();

        Mail::to($oRefer->email)->queue(new \App\Mail\SendReferToClient($oRefer));
        foreach ($oManagers as $oManager) {
            Mail::to($oManager->email)->queue(new \App\Mail\SendReferToManager($oRefer));
        }


        return responseCommon()->success([
            'view' => view('app.components.refers.success')->render(),
        ], 'Ваша заявка успешно отправлена!');
    }

    public function page(Request $request, $name)
    {
        $oPage = Page::where('name', $name)->first();
        if (is_null($oPage)) {
            abort(404);
        }
        MetaTag::setTags([
            'title' => $oPage->title,
            'description' => $oPage->title,
        ]);
        return view('app.page', [
            'oPage' => $oPage,
        ]);
    }

    public function listingShow(Request $request, $id)
    {
        $oListing = Listing::where('id', $id)->active()->first();
        if (is_null($oListing)) {
            abort(404);
        }
        MetaTag::setTags([
            'title' => $oListing->title,
            'description' => $oListing->title,
        ]);
        return view('app.listing', [
            'oListing' => $oListing,
        ]);
    }
}
