<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AdminAuthenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        $result = '';
        if (!$request->expectsJson()) {
            $result = routeCmf('auth.login', ['backTo' => url()->current()]);
        }
        return $result;
    }
}
