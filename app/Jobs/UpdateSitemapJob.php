<?php

namespace App\Jobs;

use App\Models\Listing;
use App\Models\Page;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Spatie\Sitemap\Sitemap;

class UpdateSitemapJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * UpdateSitemap constructor.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemap = Sitemap::create();

        $oPages = Page::active()->get();
        $oListings = Listing::active()->get();

        $sitemap->add(\Spatie\Sitemap\Tags\Url::create('/')->setLastModificationDate(now()));

        foreach ($oPages as $oPage) {
            $sitemap->add(\Spatie\Sitemap\Tags\Url::create($oPage->getUrl())->setLastModificationDate(now()));
        }

        foreach ($oListings as $oListing) {
            $sitemap->add(\Spatie\Sitemap\Tags\Url::create($oListing->getUrl())->setLastModificationDate(now()));
        }

        $sitemap->writeToFile(public_path('sitemap.xml'));

        Log::info('sitemap created');
    }

    /**
     * The job failed to process.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::critical($exception->getMessage());
    }
}
