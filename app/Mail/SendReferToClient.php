<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReferToClient extends Mailable
{
    use Queueable, SerializesModels;

    private $oRefer = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($oRefer)
    {
        $this->oRefer = $oRefer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'Спасибо за заявку';
        return $this->view('app.email.to_client')->with([
            'title' => $title
        ])->subject($title);
    }
}
