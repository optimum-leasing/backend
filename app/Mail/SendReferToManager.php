<?php

namespace App\Mail;

use App\Models\Refer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReferToManager extends Mailable
{
    use Queueable, SerializesModels;

    private $oRefer = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Refer $oRefer)
    {
        $this->oRefer = $oRefer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'У вас новая заявка №' . $this->oRefer->id;
        return $this->view('app.email.to_manager')->with([
            'title' => $title,
            'oRefer' => $this->oRefer,
        ])->subject($title);
    }
}
