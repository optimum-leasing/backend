<?php

namespace App\Models;

use App\Services\Modelable\Statusable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $description
 * @property int $service
 * @property int $status
 */
class Call extends Model
{
    use Statusable;

    /**
     * @var string
     */
    protected $table = 'calls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'phone', 'description', 'service', 'status',
    ];

    const STATUS_NOT_ACTIVE = 0; // не активный
    const STATUS_ACTIVE = 1; // активный
    const STATUS_VIEWED = 2; // просмотренный
    const STATUS_LATER = 3; // отложенный
    const STATUS_CLOSED = 4; // закрытый

    public $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_VIEWED => 'Просмотренный',
        self::STATUS_LATER => 'Отложен',
        self::STATUS_CLOSED => 'Закрыт',
    ];

    public $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-danger',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
        self::STATUS_VIEWED => [
            'class' => 'badge badge-success',
        ],
        self::STATUS_LATER => [
            'class' => 'badge badge-warning',
        ],
        self::STATUS_CLOSED => [
            'class' => 'badge badge-default',
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return !in_array($this->status, [
            self::STATUS_NOT_ACTIVE,
            self::STATUS_CLOSED,
        ]);
    }

    /**
     * @param string $value
     */
    public function setPhoneAttribute(string $value): void
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        $this->attributes['phone'] = $value;
    }
}
