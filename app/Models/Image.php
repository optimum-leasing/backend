<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $type
 * @property int $imageable_id
 * @property string $imageable_type
 * @property string $options
 * @property string $filename
 * @property int $is_main
 * @property int $priority
 * @property int $status
 */
class Image extends Model
{
    /**
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'imageable_id', 'imageable_type', 'options', 'filename', 'is_main', 'priority', 'status',
    ];
}
