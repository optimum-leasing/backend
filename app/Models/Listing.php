<?php

namespace App\Models;

use App\Services\Modelable\Imageable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $prepaid
 * @property int $price
 * @property int $price_up
 * @property int $contract_term_from
 * @property int $contract_term_to
 * @property int $status
 */
class Listing extends Model
{
    use Imageable;

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'listings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'prepaid', 'price', 'price_up', 'contract_term_from', 'contract_term_to', 'minimum_package', 'priority', 'status',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute(): string
    {
        return route('listing.show', [
            'id' => $this->id,
            'slug' => Str::slug($this->title),
        ]);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }
}
