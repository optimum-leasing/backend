<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $priority
 * @property int $status
 */
class ListingSubject extends Model
{
    /**
     * @var string
     */
    protected $table = 'listing_subjects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'priority', 'status',
    ];
}
