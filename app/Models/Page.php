<?php

namespace App\Models;

use App\Services\Modelable\Imageable;
use Fomvasss\LaravelMetaTags\Traits\Metatagable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property int $status
 */
class Page extends Model
{
    use Imageable;
    use Metatagable;

    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'text', 'status',
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const PAGE_ABOUT = 'about';
    const PAGE_BENEFITS = 'benefits';
    const PAGE_CONTACTS = 'contacts';

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string|null $page
     * @return array|string
     */
    public static function getPageTitles(string $page = null)
    {
        $data = [
            self::PAGE_ABOUT => 'О компании',
            self::PAGE_BENEFITS => 'Преимущества',
            self::PAGE_CONTACTS => 'Контакты',
        ];
        if (!is_null($page)) {
            return $data[$page];
        }
        return $data;
    }

    public function setTextAttribute($value)
    {
        $value = str_replace('  >', '>', $value);
        $value = str_replace(' >', '>', $value);

        $value = str_replace('<p></p>', '', $value);

        $this->attributes['text'] = $value;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute(): string
    {
        return route('page', [
            'name' => $this->name
        ]);
    }
}
