<?php

namespace App\Models;

use App\Services\Modelable\Imageable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $status
 */
class User extends Authenticatable
{
    use HasRoles;
    use Imageable;

    const ROLE_SUPER_ADMIN = 'super-admin';
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_MANAGER_A = 'manager-a';
    const ROLE_MANAGER_B = 'manager-b';

    const PERMISSION_ADMIN_VIEW = 'admin view';
    const PERMISSION_ADMIN_EDIT = 'admin edit';
    const PERMISSION_ADMIN_DELETE = 'admin delete';


    const PERMISSION_EVENT_A = 'event a';
    const PERMISSION_EVENT_B = 'event b';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(self::ROLE_SUPER_ADMIN);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === 1;
    }
}
