<?php

namespace App\Registries;

use App\Models\User;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
//use Sentinel;
use Illuminate\Support\Facades\Config;

class Member
{
    /**
     * Пользователь с которым работает данный экземпляр класса
     * @var User
     */
    public $user;

    /**
     * @var integer
     */
    public $nUserId;

    /**
     * Роль пользователя
     * @var object
     */
    private $roles;

    /**
     * Member constructor.
     */
    public function __construct($user = null)
    {
        $this->user = $user ?? Auth::user();
//        $this->roles = Cache::tags('members')->remember('user_' . $this->user->id . '-roles', 600, function () {
//            return $this->user->roles()->get()->toArray();
//        });
    }

    /**
     * Возвращает экземпляр класса для текущего авторизованного юзера.
     * @return Member
     */
    public static function current()
    {
        $user = Auth::user();
        $self = new self($user);
        return $self;
    }

    /**
     * Возвращает экземпляр класса для юзера по id
     * @param int $nId
     * @return Member
     */
    public static function getById($nId)
    {
        $user = User::find($nId);
        $self = new self($user);
        return $self;
    }


    /**
     * Возвращает всю информацию о пользователе данного экземпляра класса
     * @return \Illuminate\Support\Collection
     */
    public function get()
    {
        if (!Config::get('cmf.cache.member')) {
            $aMember = $this->getMember();
        } else {
            $aMember = Cache::tags('members')->remember('user_' . $this->user->id, 3600, function () {
                return $this->getMember();
            });
        }
        return collect($aMember);
    }

    /**
     * собирает и возращает информацию о пользователе по id
     * @return array
     */
    private function getMember()
    {
        $aResult['user'] = $this->user->toArray();
        $aResult['model'] = $this->user;
        $aResult['user']['role'] = $this->user->roles()->first()->toArray();
        //$aResult['user']['roles'] = $this->roles;
        //$aResult['role'] = $this->user->roles()->first()->toArray();
        //$aResult['roles'] = $this->roles;
        $aResult['user']['image'] = ImagePath::main('user', 'square', $this->user);
        $aResult['permission'][User::PERMISSION_ADMIN_VIEW] = $this->user->hasPermissionTo(User::PERMISSION_ADMIN_VIEW);
        $aResult['permission'][User::PERMISSION_ADMIN_EDIT] = $this->user->hasPermissionTo(User::PERMISSION_ADMIN_EDIT);
        $aResult['permission'][User::PERMISSION_ADMIN_DELETE] = $this->user->hasPermissionTo(User::PERMISSION_ADMIN_DELETE);

        return $aResult;
    }

    public function canAdminView()
    {
        return MemberRegistry::getInstance()->get('permission.' . User::PERMISSION_ADMIN_VIEW);
    }

    public function canAdminEdit()
    {
        return MemberRegistry::getInstance()->get('permission.' . User::PERMISSION_ADMIN_EDIT);
    }

    public function canAdminDelete()
    {
        return MemberRegistry::getInstance()->get('permission.' . User::PERMISSION_ADMIN_DELETE);
    }

    public function isSuperAdmin()
    {
        return $this->user->hasRole(User::ROLE_SUPER_ADMIN);
    }

    public function hasRole($array)
    {
        return $this->user->hasAnyRole($array);
    }
}
