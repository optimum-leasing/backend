<?php

namespace App\Services\Image\Filters;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\Services\Image\Filters\FilterTrait;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class SquareFilter implements FilterInterface
{
    use FilterTrait;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var Image
     */
    private $image = null;

    /**
     * @var int
     */
    protected $size = null;

    /**
     * SquareFilter constructor.
     * @param array $options
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * @param Image $image
     * @return Image|mixed
     */
    public function applyFilter(Image $image)
    {
        if (isset($this->options['dimension'])) {
            $width = $image->width();
            $height = $image->height();
            $dimension = explode(':', $this->options['dimension']);
            $q = $dimension[1] / $dimension[0];
            if ($width > $height) {
                $image = $image->fit(intval($height), intval($height * $q));
            } else {
                $image = $image->fit(intval($width), intval($width * $q));
            }
        } else {
            $this->size = intval($this->options['size']);
            $image = $this->fit($image, $this->size);
        }
        if (isset($this->options['size'])) {
            $this->size = intval($this->options['size']);
            $image = $this->fit($image, $this->size);
        }
        return $image;
    }

    /**
     * @param string $sFileName
     * @param string $originalPath
     * @param string $path
     * @param string $key
     */
    public function resize(string $sFileName, string $originalPath, string $path, string $key)
    {
        $this->image = ImageStatic::make($originalPath . $sFileName);
        $this->image = $this->applyFilter($this->image);
        $sPath = public_path($path . '/' . $key . '/');
        $this->checkDirectory($sPath);
        $this->image->save($sPath . $sFileName);

        if (config('image.cloud')) {
            if (file_exists($sPath . $sFileName)) {
                $path = str_replace('storage', '', $path);
                $fileCloud = Storage::disk('yandex')->put($path . '/' . $key . '/' . $sFileName, File::get($sPath . $sFileName));
            }
        }
    }

    /**
     * @param Image $image
     * @param int $width
     * @return mixed
     */
    private function fit(Image $image, $width)
    {
        $image = $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $image;
    }
}
