<?php

namespace App\Services\Image;

use App\Models\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImagePath extends ImageService
{
    /**
     * Директория в public, где изображнения для сущностей по умолчанию
     *
     * @var string
     */
    private $defaultDirectory = 'img/default';

    /**
     * Изображение по умолчанию
     *
     * @param string $key
     * @param string|null $size
     * @return string
     */
    public function default(string $key = 'default', string $size = null): string
    {
        if (!is_null($size)) {
            $file = $this->defaultDirectory . '/' . $key . '_' . $size . '.png';
            if (file_exists(public_path($file))) {
                return $this->defaultKeyAsset($key . '_' . $size);
            }
        }
        $file = $this->defaultDirectory . '/' . $key . '.png';
        if (!file_exists(public_path($file))) {
            $key = 'default';
        }
        return $this->defaultKeyAsset($key);
    }

    /**
     * Изоюражение по умолчанию по ключу
     *
     * @param string $key {user}
     * @return string
     */
    private function defaultKeyAsset(string $key): string
    {
        return $this->asset($this->defaultDirectory . '/' . $key . '.png');
    }

    /**
     * Главное изображение сущности
     *
     * @param string $key
     * @param string $size
     * @param Image|null $model
     * @return string
     */
    public function image(string $key, string $size, Image $model = null): string
    {
        if (is_null($model)) {
            return $this->default($key, $size);
        }
        // значит вид ключа news/text
        if (strpos($key, '/') !== false) {
            $aKey = explode('/', $key);
            $key = $aKey[0] ?? $key;
            $this->setPrefix($aKey[1]);
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->default($key, $size);
        }
        $path = $this->createPath($key, $size, $model->imageable_id, $filename);

        if (config('image.cloud')) {
            $pathCloud = str_replace('storage/', '', $path);
            if (Storage::disk('yandex')->exists($pathCloud)) {
                return $this->returnCloud($pathCloud);
            }
        }
        return $this->asset($path);
    }

    /**
     * @param string $key
     * @param string $size
     * @param null|object $model
     * @return string
     */
    public function main(string $key, string $size, $model = null): string
    {
        if (!$oFile = $this->checkMainImage($model)) {
            return $this->default($key, $size);
        }
        $filename = $oFile->filename;
        $path = $this->createPath($key, $size, $model->id, $filename);

        if (config('image.cloud')) {
            $pathCloud = str_replace('storage/', '', $path);
            if (Storage::disk('yandex')->exists($pathCloud)) {
                return $this->returnCloud($pathCloud);
            }
        }
        return $this->checkFile($path) ? $this->asset($path) : $this->default($key, $size);
    }

    private function returnCloud($pathCloud)
    {
        $command = Storage::disk('yandex')->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key' => $pathCloud,
            //'ResponseContentDisposition' => 'attachment;' // чтобы сказать
        ]);
        $request = Storage::disk('yandex')->getDriver()->getAdapter()->getClient()->createPresignedRequest($command, '+5 minutes');
        return (string) $request->getUri();
    }

    /**
     * @param string $key
     * @param string $size
     * @param int $id
     * @param string $filename
     * @return string
     */
    private function createPath(string $key, string $size, int $id, string $filename): string
    {
        return $this->getPathWithPrefix($key, $id) . '/' . $size . '/' . $filename;
        //return $this->imagePublicDirectory . '/' . $key . '/' . $id . '/' . $size . '/' . $filename;
    }

    /**
     * @param string $key
     * @param string $size
     * @param null|object $model
     * @return bool
     */
    public function checkMain(string $key, string $size, $model = null): bool
    {
        if (!$oFile = $this->checkMainImage($model)) {
            return false;
        }
        $filename = $oFile->filename;
        $path = $this->createPath($key, $size, $model->id, $filename);
        if (!$this->checkFile($path)) {
            return false;
        }
        return true;
    }

    /**
     * @param null|object $model
     * @return null|bool|Image
     */
    private function checkMainImage($model)
    {
        if (is_null($model)) {
            return false;
        }
        $images = $model->images;
        if (is_null($images) || empty($images) || empty($images[0])) {
            return false;
        }
        $oFile = $images->where('is_main', 1)->first();
        if (is_null($oFile)) {
            return false;
        }
        return $oFile;
    }

    /**
     * @param string $key
     * @param string $size
     * @param Image $model
     * @return string
     */
    public function publicPath(string $key, string $size, Image $model): string
    {
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return public_path($this->default($key));
        }
        $path = $this->createPath($key, $size, $model->imageable_id, $filename);
        return public_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    private function asset(string $path): string
    {
        return $this->remoteUrl($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    private function checkFile(string $path): bool
    {
        return File::exists(public_path($path));
    }

    /**
     * @param string $path
     * @return string
     */
    private function remoteUrl(string $path): string
    {
        return config('image.url.remote') . '/' . $path;
    }
}
