<?php

namespace App\Services\Image;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    /**
     * Директория с оригиналами
     *
     * @var string
     */
    protected $originalPath = '';

    /**
     * Ключ с оригиналами
     *
     * @var string
     */
    protected $originalKey = 'original';

    /**
     * Директория куда загружаются все изображения сущности
     *
     * @var string
     */
    protected $path;

    /**
     * Название файла
     *
     * @var string
     */
    public $sFileName = '';

    /**
     * @var bool
     */
    public $isTmp = false;

    /**
     * @var null|string
     */
    private $prefix = null;

    /**
     * images
     *
     * @var string
     */
    protected $imagePublicDirectory;

    /**
     * @var string
     */
    protected $imageStorageTmpDirectory;

    /**
     * ImageService constructor.
     */
    public function __construct()
    {
        $this->imagePublicDirectory = config('cmf.image_public_directory');

        if (config('app.env') === 'testing') {
            $this->imagePublicDirectory = config('cmf.image_testing_directory');
        }
        $this->imageStorageTmpDirectory = 'storage/tmp';
    }

    /**
     * Загрузить и обрезать
     * @param object|string|null $file
     * @param string $sType
     * @param int $nId
     * @param array $options
     * @param null|string $fileFull
     * @param string $fileName
     * @return string
     */
    public function upload($file, $sType, $nId, $options = null, $fileFull = null, $fileName = null)
    {
        $this->path = $this->getPathWithPrefix($sType, $nId);
        $this->originalPath = public_path($this->path . '/' . $this->originalKey . '/');

        if (!File::exists($this->originalPath)) {
            File::makeDirectory($this->originalPath, 0777, true);
        }
        if (!is_null($fileFull)) {
            $sFileName = str_random(12) . '' . substr(strrchr($fileName, '.'), 0);
            File::copy($fileFull, $this->originalPath . $sFileName);
            $this->sFileName = $sFileName;
        } else {
            $this->sFileName = $this->uploadOriginalFile($file, $this->originalPath);
        }

        if (!is_null($options)) {
            foreach ($options as $key => $option) {
                $oObject = new $option['filter']($option['options']);
                $oObject->resize($this->sFileName, $this->originalPath, $this->path, $key);
            }
        }

        return $this->sFileName;
    }

    /**
     * Режим с записью в storage/tmp
     */
    public function setTemp(): void
    {
        $this->isTmp = true;
    }

    /**
     * Режим в запись в public
     */
    public function setNotTemp(): void
    {
        $this->isTmp = false;
    }

    /**
     * @param string $type
     * @param string|int $id
     * @param string $filename
     * @return string
     */
    public function getAssetToOriginal($type, $id, $filename)
    {
        return $this->getPathWithPrefix($type, $id) . '/original/' . $filename;
    }

    /**
     * Загрузить оригинальное изображение
     * @param object $file
     * @param string $path
     * @return string
     */
    public function uploadOriginalFile($file, $path): string
    {
        $sFileName = $file->getClientOriginalName();
        $sFileName = str_random(12) . '' . $this->getExtension($sFileName);
        $file instanceof \Illuminate\Http\UploadedFile ? $this->moveUploadedFile($file, $path, $sFileName) : $file->move($path, $sFileName);
        return $sFileName;
    }

    public function getExtension($sFileName)
    {
        return substr(strrchr($sFileName, '.'), 0);
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $path
     * @param string $sFileName
     */
    private function moveUploadedFile(\Illuminate\Http\UploadedFile $file, $path, $sFileName): void
    {
        File::copy($file->getPathName(), $path . $sFileName);

        if (config('image.cloud')) {
            $pathCloud = str_replace('storage', '', $this->path);
            $fileCloud = Storage::disk('yandex')->put($pathCloud . '/' . $this->originalKey . '/' . $sFileName, File::get($file));
        }
    }

    /**
     * @return string
     */
    private function path(): string
    {
        return $this->isTmp ? $this->imageStorageTmpDirectory : $this->imagePublicDirectory;
    }

    /**
     * Удалить изображение со всех папок
     * @param string $sFileName
     * @param string $sType
     * @param array $options
     * @param string|int $nId
     */
    public function deleteImages($sFileName, $sType, $nId, $options = null)
    {
        $aSizes[] = $this->originalKey;
        $path = $this->getPathWithPrefix($sType, $nId);

        if (!is_null($options)) {
            foreach ($options['filters'] as $key => $option) {
                $aSizes[] = $key;
            }
        }
        foreach ($aSizes as $size) {
            $dir = public_path($path . '/' . $size);
            $file = public_path($path . '/' . $size . '/' . $sFileName);
            if (file_exists($file)) {
                File::Delete($file);

                if (config('image.cloud')) {
                    $pathCloud = str_replace('storage', '', $path);
                    $fileCloud = $pathCloud . '/' . $size . '/' . $sFileName;
                    if (Storage::disk('yandex')->exists($fileCloud)) {
                        $fileCloud = Storage::disk('yandex')->delete($fileCloud);
                    }
                    $this->deleteDirectory($dir);
                }
            }
        }
        if (File::exists(public_path($path))) {
            $this->deleteDirectory(public_path($path));
        }
    }

    /**
     * @param string $sType
     * @param string|int $nId
     * @return string
     */
    protected function getPathWithPrefix($sType, $nId)
    {
        if (!empty($this->prefix)) {
            if (strpos($sType, $this->prefix) !== false) {
                $sType = str_replace('/' . $this->prefix, '', $sType);
            }
            return $this->path() . '/' . $sType . '/' . $nId . '/' . $this->prefix;
        }
        return $this->path() . '/' . $sType . '/' . $nId;
    }

    /**
     * Удалить директорию, если в ней нет файлов
     *
     * @param string $dir
     */
    private function deleteDirectory(string $dir): void
    {
        if (count(File::allFiles($dir)) === 0) {
            File::deleteDirectory($dir);
        }
    }

    /**
     * @param string $name
     */
    public function setPrefix(string $name): void
    {
        $this->prefix = $name;
    }

    /**
     * @param string $sType
     * @param string|integer $nId
     */
    public function deleteDirectoryByParameters($sType, $nId): void
    {
        $path = $this->getPathWithPrefix($sType, $nId);
        $dir = public_path($path);
        File::deleteDirectory($dir);
    }
}
