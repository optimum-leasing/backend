<?php

namespace App\Services\Modelable;

use App\Models\Image;
use App\Services\Image\Facades\ImagePath;

trait Imageable
{
    /**
     * Магический метод, для получения пути картинок
     * @param  string $name             формат картинки
     * @return string | null | object   путь до изображения
     */
    public function __get($name)
    {
        switch ($name) {
            case 'image':
                return $this->getImagePath('original');
            case 'image_square':
                return $this->getImagePath('square');
            case 'image_rectangle_height':
                return $this->getImagePath('rectangle_height');
            default:
                return parent::__get($name);
        }
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->where('type', $this->getImageTypeModel());
    }

    public function textImages()
    {
        return $this->morphMany(Image::class, 'imageable')->where('type', $this->getImageTypeModel() . '/text');
    }

    /**
     * @param string $type
     * @return string
     */
    public function getImagePath($type = 'square'): string
    {
        $model = $this->getImageTypeModel();
        return ImagePath::main($model, $type, $this);
    }

    /**
     * @param string $type
     * @return bool
     */
    public function hasImagePath($type = 'square'): bool
    {
        $model = $this->getImageTypeModel();
        return ImagePath::checkMain($model, $type, $this);
    }

    /**
     * @return string
     */
    private function getImageTypeModel(): string
    {
        $model = get_class($this);
        $model = strtolower(substr($model, strrpos($model, '\\') + 1));
        return $model;
    }
}
