<?php

namespace App\Services\Modelable;

trait Statusable
{
    /**
     * @return array
     */
    public function statuses(): array
    {
        return property_exists($this, 'statuses') ? $this->statuses : [
            0 => 'Не активно',
            1 => 'Активно',
        ];
    }

    /**
     * @return array
     */
    public function statusIcons(): array
    {
        return property_exists($this, 'statusIcons') ? $this->statusIcons : [
            0 => [
                'class' => 'badge badge-default',
            ],
            1 => [
                'class' => 'badge badge-success',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function staticStatuses(): array
    {
        return (new self)->statuses();
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getStatuses()
    {
        return $this->statuses();
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return $this->statuses()[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return array
     */
    public function getStatusIconAttribute(): array
    {
        $this->statusIcons()[$this->status]['title'] = $this->statuses()[$this->status];
        return $this->statusIcons()[$this->status];
    }
}
