<?php

return [

    'version' => '0.0.1',

    'as' => env('CMF_AS', ''),

    'url' => env('CMF_URL', env('APP_URL', '')),

    'prefix' => env('CMF_PREFIX', ''),

    'image_public_directory' => env('IMAGE_PUBLIC_DIRECTORY', 'storage/images'),

    'image_testing_directory' => env('IMAGE_TESTING_DIRECTORY', 'storage/testing/images'),

    'files_public_directory' => env('FILES_PUBLIC_DIRECTORY', 'storage/files'),

    'files_testing_directory' => env('FILES_TESTING_DIRECTORY', 'storage/testing/files'),

    'sidebar' => [
        'admin' => [
            '' => [
                'title' => 'Главная',
                'iconCls' => 'icon-puzzle',
            ],
//            \App\Cmf\Project\User\UserController::NAME => [
//                'title' => \App\Cmf\Project\User\UserController::TITLE,
//                'iconCls' => \App\Cmf\Project\User\UserController::ICON,
//            ],
            \App\Cmf\Project\Page\PageController::NAME => [
                'title' => \App\Cmf\Project\Page\PageController::TITLE,
                'iconCls' => \App\Cmf\Project\Page\PageController::ICON,
                'roles' => [
                    \App\Models\User::ROLE_SUPER_ADMIN
                ]
            ],
            \App\Cmf\Project\Listing\ListingController::NAME => [
                'title' => \App\Cmf\Project\Listing\ListingController::TITLE,
                'iconCls' => \App\Cmf\Project\Listing\ListingController::ICON,
                'roles' => [
                    \App\Models\User::ROLE_SUPER_ADMIN
                ]
            ],
            \App\Cmf\Project\Call\CallController::NAME => [
                'title' => \App\Cmf\Project\Call\CallController::TITLE,
                'iconCls' => \App\Cmf\Project\Call\CallController::ICON,
            ],
        ],
    ],

    'options' => [
        'app' => [
            'title' => 'Административная панель',
            'title_short' => env('APP_NAME'),
            'description' => null,
            'keywords' => null,
            'favicon' => [
                'main' => '/img/logo.png',
            ],
            'powered' => env('APP_NAME'),
            'copyright' => ' ' . env('APP_NAME'),
        ],
    ],

    'cache' => [
        'member' => true,

        'name' => 'user_',
        'tag' => 'members',
    ]
];
