<?php

if (!function_exists('responseCommon')) {

    function responseCommon()
    {
        return (new \App\Services\ResponseCommon\ResponseCommonHelpers());
    }
}

if (!function_exists('routeCmf')) {
    /**
     * Generate the URL to a named route.
     *
     * routeCmf('create.modal.post')
     *
     * @param string $view
     * @param array $parameters
     * @param bool $absolute
     * @return string
     * @see \App\Cmf\Core\RouteCmf::resource()
     *
     */
    function routeCmf($view, $parameters = [], $absolute = true)
    {
        $prefix = config('cmf.as');
        if ($prefix !== '') {
            $view = $prefix . '.' . $view;
        }
        return app('url')->route($view, $parameters, $absolute);
    }
}

if (!function_exists('viewAdminContent')) {
    /**
     * Generate the URL to a named route.
     *
     * viewAdminContent('components.table')
     *
     * @param string $name
     * @return string
     * @see \App\Cmf\Core\RouteCmf::resource()
     *
     */
    function viewAdminContent($name)
    {
        $view = 'admin.content.';

        if (!is_null(\Illuminate\Support\Facades\Route::current())) {
            $view = $view . stristr(\Illuminate\Support\Facades\Route::current()->getName(), '.', true) . '.' . $name;
        }

        return $view;
    }
}

if (!function_exists('cmfHelper')) {

    function cmfHelper($type = 'cmf')
    {
        return new \App\Cmf\Core\Helper($type);
    }
}

if (!function_exists('apiRoutes')) {

    function apiRoutes()
    {
        $routes = [];
        app()->singleton('api.router', function ($app) use (&$routes) {
            $router = new \Dingo\Api\Routing\Router(
                $app[\Dingo\Api\Contract\Routing\Adapter::class],
                $app[\Dingo\Api\Contract\Debug\ExceptionHandler::class],
                $app,
                config('api.domain'),
                config('api.prefix')
            );
            $router->setConditionalRequest(config('api.conditionalRequest'));
            $routes = $router->getRoutes();
            return $router;
        });
        dd($routes);
        return $routes;
    }
}

if (!function_exists('formatBytes')) {

    function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}

if (!function_exists('member')) {

    function member() {
        return new \App\Registries\Member();
    }
}

if (!function_exists('strpos_all')) {
    /**
     * Все позиции strpos
     *
     * @param string $haystack
     * @param string $needle
     * @return array
     */
    function strpos_all($haystack, $needle)
    {
        $offset = 0;
        $allpos = [];
        while (($pos = mb_strpos($haystack, $needle, $offset)) !== false) {
            $offset = $pos + 1;
            $allpos[] = $pos;
        }
        return $allpos;
    }
}

