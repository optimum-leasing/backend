<?php

use App\Models\Call;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(Call::class, function (Faker $faker) use ($fakerRU) {
    return [
        'name' => $fakerRU->firstName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'status' => rand(0, 4),
    ];
});
