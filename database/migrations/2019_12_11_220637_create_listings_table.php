<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->tinyInteger('prepaid')->nullable()->default(null); // аванс в процентах
            $table->bigInteger('price')->nullable()->default(null); // Стоимость оборудовани
            $table->integer('price_up')->nullable()->default(null); // Удорожание
            $table->integer('contract_term_from')->nullable()->default(null); // Срок договора лизинга от
            $table->integer('contract_term_to')->nullable()->default(null); // Срок договора лизинга до
            $table->string('minimum_package')->nullable()->default(null); // Минимальный пакет документов
            $table->integer('early_redemption')->nullable()->default(null); // Минимальный пакет документов
            $table->integer('has_payments_types')->unsigned()->default(0);
            $table->integer('priority')->unsigned()->default(0);
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
