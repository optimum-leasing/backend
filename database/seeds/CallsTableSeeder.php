<?php

use App\Models\Call;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Page;

class CallsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
        factory(Call::class)->create();
    }
}
