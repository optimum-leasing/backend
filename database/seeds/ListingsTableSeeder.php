<?php

use App\Models\Listing;
use App\Models\ListingSubject;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Page;
use App\Database\Seeds\CommonDatabaseSeeder;

class ListingsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Лизинг оборудования',
                'description' => 'Лизинг оборудования позволяет компании в короткий срок увеличить эффективность производства, снизить издержки, тем самым повысить свою конкурентоспособность.',
                'prepaid' => 20,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 60,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
                'has_payments_types' => 1,
            ], [
                'title' => 'Лизинг спецтехники',
                'description' => 'Лизинг спецтехники позволит организации оперативно обновить или пополнить материально-техническую базу. С учетом высокой стоимости такой техники, оформление ее в лизинг экономически выгодно.',
                'prepaid' => 0,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 60,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
                'has_payments_types' => 1,
            ], [
                'title' => 'Автолизинг',
                'description' => 'Лизинг легкового и коммерческого автотранспорта — эффективный способ обновления или расширения автопарка.',
                'prepaid' => 0,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 60,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
            ], [
                'title' => 'Лизинг недвижимости',
                'description' => 'Купить недвижимое имущество для ведения коммерческой деятельности без отвлечения собственных финансовых ресурсов – реальная возможность, которую предоставляет лизинг недвижимости для юридических лиц.',
                'prepaid' => 0,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 60,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 12,
            ], [
                'title' => 'Лизинг грузового транспорта',
                'description' => 'Лизинг грузового транспорта — это отличная возможность для многих предприятий расширить или обновить свой автопарк.',
                'prepaid' => 0,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 60,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
            ], [
                'title' => 'Лизинг подержанной техники / оборудования',
                'description' => 'Лизинговая компания Страйк предоставляет своим клиентам лизинг как новой, так и бывшей в эксплуатации техники/оборудования/недвижимости.',
                'prepaid' => 0,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 36,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
            ], [
                'title' => 'Возвратный Лизинг',
                'description' => 'Возвратный лизинг – возможность быстро пополнить оборотные средства предприятия и расширить свои финансовые возможности.',
                'prepaid' => null,
                'price' => 1000000,
                'price_up' => 8,
                'contract_term_from' => 12,
                'contract_term_to' => 36,
                'minimum_package' => 'Учредительные документы, финансовая отчетность',
                'early_redemption' => 6,
                'subjects' => [
                    [
                        'title' => 'коммерческая недвижимость',
                    ], [
                        'title' => 'автомобили',
                        'description' => 'грузовые, легковые',
                    ], [
                        'title' => 'техника',
                    ], [
                        'title' => 'оборудование',
                    ]
                ]
            ]
        ];
        $totalListings = count($data);
        foreach ($data as $key => $value) {
            $subjects = [];
            if (isset($value['subjects'])) {
                $subjects = $value['subjects'];
                unset($value['subjects']);
            }
            $value['priority'] = $this->priority($key, $totalListings);
            $oListing = Listing::create($value);

            if (!empty($subjects)) {
                $totalSubjects = count($subjects);
                foreach ($subjects as $keySubject => $subject) {
                    $subject['listing_id'] = $oListing->id;
                    $subject['priority'] = $this->priority($keySubject, $totalSubjects);
                    ListingSubject::create($subject);
                }
            }
        }
    }
}
