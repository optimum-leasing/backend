<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sAdmin = factory(\Spatie\Permission\Models\Role::class)->create([
            'name' => User::ROLE_SUPER_ADMIN,
            'title' => 'Супер Администратор',
        ]);
        $managerA = factory(\Spatie\Permission\Models\Role::class)->create([
            'name' => User::ROLE_MANAGER_A,
            'title' => 'Организатор А',
        ]);
        $managerB = factory(\Spatie\Permission\Models\Role::class)->create([
            'name' => User::ROLE_MANAGER_B,
            'title' => 'Организатор Б',
        ]);
        $view = factory(\Spatie\Permission\Models\Permission::class)->create([
            'name' => User::PERMISSION_ADMIN_VIEW,
            'title' => 'Возможность просмотра',
        ]);
        $edit = factory(\Spatie\Permission\Models\Permission::class)->create([
            'name' => User::PERMISSION_ADMIN_EDIT,
            'title' => 'Возможность редактирования',
        ]);
        $delete = factory(\Spatie\Permission\Models\Permission::class)->create([
            'name' => User::PERMISSION_ADMIN_DELETE,
            'title' => 'Возможность удалять',
        ]);

        $eventA = factory(\Spatie\Permission\Models\Permission::class)->create([
            'name' => User::PERMISSION_EVENT_A,
            'title' => 'Управлять событиями А',
        ]);
        $eventB = factory(\Spatie\Permission\Models\Permission::class)->create([
            'name' => User::PERMISSION_EVENT_B,
            'title' => 'Управлять событиями Б',
        ]);

        $sAdmin->givePermissionTo($view);
        $sAdmin->givePermissionTo($edit);
        $sAdmin->givePermissionTo($delete);

        $managerA->givePermissionTo($view);
        $managerA->givePermissionTo($eventA);

        $managerB->givePermissionTo($view);
        $managerB->givePermissionTo($eventB);

    }
}
