<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // супер админ
        $admin = User::create([
            'name' => 'Админ',
            'email' => 'admin@admin.com',
            'password' => Hash::make('1234567890'),
        ]);
        $admin->assignRole(User::ROLE_SUPER_ADMIN);

        // организатор
        $user = factory(User::class)->create([
            'password' => Hash::make('1234567890'),
        ]);
        $user->assignRole(User::ROLE_MANAGER_A);

        // организатор
        $user = factory(User::class)->create([
            'password' => Hash::make('1234567890'),
        ]);
        $user->assignRole(User::ROLE_MANAGER_B);
    }
}
