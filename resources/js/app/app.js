/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': window.Laravel}
});

require('./package/jquery.ajaxchimp.min.js');
//require('./package/owl.carousel.min.js');
//require('./package/jquery.nice-select.min.js');
//require('./package/jquery.magnific-popup.min.js');
//require('./package/jquery.counterup.min.js');
//require('./package/waypoints.min.js');
require('./main.js');

require('./../common/fancybox/fancybox.js');
require('./../common/masks/cleave-masks.js');
require('./../common/notification/notification.js');
require('./../cmf/project/form.js');
require('./../cmf/project/callbacks/form.js');
