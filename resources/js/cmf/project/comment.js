

$(document).ready(function () {


    $('body').on('click', '.toggle-item', function () {
        let $button = $(this);
        let $item = $($button.data('item') + '.--on');
        let $itemOff = $($button.data('item') + '.--off');
        if ($item.hasClass('hidden')) {
            $item.removeClass('hidden');
            if ($button.data('icon-before') && $button.data('icon-after')) {
                $button.find('.fa').removeClass($button.data('icon-before'));
                $button.find('.fa').addClass($button.data('icon-after'));
            }
            if ($itemOff.length) {
                $itemOff.addClass('hidden');
            }
            if ($button.data('hide')) {
                $button.addClass('hidden');
            }
        } else {
            $item.addClass('hidden');
            if ($button.data('icon-before') && $button.data('icon-after')) {
                $button.find('.fa').removeClass($button.data('icon-after'));
                $button.find('.fa').addClass($button.data('icon-before'));
            }
            if ($itemOff.length) {
                $itemOff.removeClass('hidden');
            }
            $('.toggle-item[data-item="' + $button.data('item') + '"]').removeClass('hidden');
        }
        if ($button.data('reset-from') && $button.data('reset-to')) {
            $($button.data('reset-to')).val($($button.data('reset-from')).val());
        }
    });
});
