

let journalPeriodEvents = {

    $select: null,
    $months: null,
    $options: null,
    $firstOption: null,
    initial: false,
    $yearNumbers: null,
    aYearNumbers: [],
    half: null,

    /**
     *
     * @param $select
     * @param initial
     * @returns {journalPeriodEvents}
     */
    init($select, initial) {
        const self = this;
        self.$select = $select;
        self.initial = initial;
        self.$months = $('.--period-month[data-year="' + self.$select.data('year') + '"]');
        self.$options = self.$months.find('option');
        self.$yearNumbers = $('#period-tab-' + self.$select.data('year').substr(0, 4)).find('.--year-numbers');
        self.half = parseInt(self.$select.data('year').substr(-1));
        return self;
    },

    /**
     *
     */
    refresh() {
        const self = this;
        //self.$months.selectpicker('deselectAll');
        if (self.initial !== undefined && !self.initial) {
            self.$months.selectpicker('val', 0);
        }
        self.$months.selectpicker('refresh');
    },

    /**
     *
     * @returns {null|*}
     */
    getFirstOption() {
        const self = this;
        self.$options.each(function (key, value) {
            if (!$(this).hasClass('hidden')) {
                self.$firstOption = $(this);
                return false;
            }
        });
        return self.$firstOption;
    },

    /**
     *
     */
    disableMonths(status) {
        const self = this;
        self.$months.prop('disabled', status);
    },

    /**
     *
     */
    diselect() {
        const self = this;
        self.$months.selectpicker('deselectAll');
    },

    /**
     *
     * @param count
     */
    eachOptions(count) {
        const self = this;
        self.$options.each(function (key, value) {
            count !== undefined && key > count ? self.optionHide($(this)) : self.optionShow($(this));
        });
    },

    /**
     *
     * @param $option
     */
    optionShow($option) {
        $option.removeClass('hidden');
    },

    /**
     *
     * @param $option
     */
    optionHide($option) {
        $option.addClass('hidden');
        $option.prop('selected', false);
    },

    /**
     *
     */
    setNumbers() {
        const self = this;
        self.$yearNumbers.find('.half-' + self.half).text(self.aYearNumbers.join(' '));
    },

    /**
     *
     * @param value
     * @param valueSecond
     */
    saveValueToNumber(value, valueSecond) {
        const self = this;
        self.aYearNumbers = self.half === 1 ? value : valueSecond;
    }
};

export {journalPeriodEvents};
