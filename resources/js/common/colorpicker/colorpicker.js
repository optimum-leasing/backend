

const Huebee = require('huebee/dist/huebee.pkgd.min');


$(document).ready(function ($) {
    window['colorpicker']();
});

window['colorpicker'] = function () {
    if ($('.color-input').length) {
        let hueb = new Huebee('.color-input', {
            // options
            notation: 'hex',
            saturations: 0,
            hues: 1,
            // staticOpen: true,
            customColors: ['#F13D37', '#7FBFFB', '#7FB6B7', '#E66FC7', '#D24861', '#1DA25D', '#E8CA20']
        });
    }
};
