
const MarkdownIt = require('markdown-it')({
    html: true,
    linkify: true,
    typographer: true
});

$(document).ready(function () {
    window['markdown']();
});

window['markdown'] = function () {
    $('.markdown').each(function () {
        let $textarea = $(this);
        let $parent = $textarea.parent();
        $parent.css('position', 'relative');
        let $preview = $parent.find('.markdown-preview');
        let value = $textarea.val().trim();
        $textarea.val(value);
        $(this).on('keyup change', function() {
            let value = $textarea.val();
            let result = MarkdownIt.render(value);
            $preview.html(result);
        });
        $textarea.trigger('change');
    });
    $('.markdown-toggler').off().click(function() {
        let $container = $(this).closest('.markdown-container');
        let $window = $container.find('.markdown-window');
        $window.toggleClass('hidden');
        $(this).toggleClass('btn-primary text-white text-black');
    });
    $('.markdown-view').each(function () {
        $(this).html(MarkdownIt.render($(this).html()));
    });
};
