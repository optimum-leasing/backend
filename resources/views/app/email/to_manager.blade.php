@extends('app.email.layouts.app')

@section('content')
    <p style="text-align: center; font-size: 20px;" ><b>Новая заявка</b></p>
    <p style="text-align: center"><b>ID:</b> {{ $oRefer->id }}</p>
    <p style="text-align: center"><b>Имя:</b> {{ $oRefer->first_name }}</p>
    <p style="text-align: center"><b>Фамилия:</b> {{ $oRefer->last_name }}</p>
    <p style="text-align: center"><b>Email:</b> {{ $oRefer->email }}</p>
    <p style="text-align: center"><b>Телефон:</b> {{ $oRefer->phone }}</p>
    <p style="text-align: center"><b>Уровень:</b> {{ $oRefer->getLevels($oRefer->level) }}</p>
@endsection
