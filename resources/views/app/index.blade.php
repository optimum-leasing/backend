@extends('app.layouts.app')

@section('content')
    @include('app.layouts.components.banner')
{{--    @include('app.layouts.components.video')--}}
{{--    @include('app.layouts.components.about')--}}
    <!-- About Generic Start -->

    <section class="about-generic-area">
        <div class="container border-top-generic --page-html-content">
{{--            {!! $oPage->text !!}--}}
{{--            <h3 class="about-title mb-70">Lorem ipsum dolor sit.</h3>--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="img-text">--}}
{{--                        <img src="/img/a3.jpg" alt="" class="img-fluid float-left mr-20 mb-20" data-fancybox="gallery-post" href="/img/a3.jpg" style="width: 350px;">--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid asperiores assumenda atque commodi consequatur, dolorem ducimus eos, illo magni nihil nisi pariatur, quaerat quas quidem quis reiciendis tenetur totam! Aliquam cumque dicta eligendi enim esse hic inventore, modi necessitatibus nobis nulla officia porro praesentium quae qui quo recusandae rem repellendus suscipit vel velit. Amet, commodi corporis delectus dolor, eaque et expedita explicabo fugiat ipsam labore neque quia rerum saepe sequi, similique. Alias animi atque dignissimos dolore fuga libero nam possimus repellendus veniam voluptatum. Corporis laborum molestiae rerum? Aperiam atque nam officia porro sit! Nisi, possimus veritatis? Accusantium consequatur cupiditate dignissimos ea ex exercitationem facilis laborum laudantium minima modi necessitatibus nisi nobis odit, officia optio perferendis qui quod similique temporibus, voluptas.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-12">--}}
{{--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, itaque ratione. Amet doloribus, et laboriosam mollitia nemo obcaecati. Architecto, aspernatur atque deleniti dolorum nulla possimus ut. Placeat quis quisquam tenetur. Ad adipisci amet corporis cumque error facilis fuga fugit magni numquam omnis quod sint, tempora velit! Deleniti dolor et excepturi neque quos reprehenderit tenetur! Ab accusamus adipisci aliquid assumenda autem blanditiis cupiditate deserunt error esse eum eveniet excepturi explicabo illo inventore iste libero maiores minima modi molestiae neque numquam optio possimus quas quis, reprehenderit saepe sequi soluta tenetur ullam vel velit vero voluptate voluptatem. Deserunt dicta esse pariatur quae quas vero voluptates. Alias atque consequatur cum distinctio ducimus eos esse eveniet, incidunt maxime qui quisquam repellendus sit soluta unde, voluptas.</p>--}}
{{--                </div>--}}
{{--                <div class="col-lg-12">--}}
{{--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, asperiores, consequuntur dolore magni molestiae natus pariatur quam quis ratione recusandae saepe sapiente! A adipisci assumenda delectus dignissimos doloribus dolorum earum et eum expedita, fuga fugiat fugit hic id maxime minima nesciunt nisi non odit officia perferendis quam quas quasi quis quo quod soluta, tempora temporibus unde vel voluptatem. Deleniti quidem rem tempore. Adipisci alias aliquid architecto autem delectus, doloribus earum eius, enim, eos error esse est eum laudantium nobis perspiciatis praesentium quaerat quas quisquam totam unde velit veniam voluptates. Deleniti error exercitationem facere quis ratione saepe similique, tenetur vero voluptatibus voluptatum! Ad amet assumenda debitis eaque earum harum, iure labore natus obcaecati quaerat quasi quibusdam repudiandae sequi sunt tenetur velit!</p>--}}
{{--                </div>--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="img-text">--}}
{{--                        <img src="/img/a4.jpg" alt="" class="img-fluid float-left mr-20 mb-20" data-fancybox="gallery-post" href="/img/a4.jpg" style="width: 350px;">--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, at dicta earum eligendi excepturi iusto labore magnam molestias neque nesciunt nihil, non nulla quia quisquam ratione sapiente unde veritatis voluptatem. A deserunt dolorem eaque impedit inventore nesciunt officiis optio repellat? Et laudantium perferendis quibusdam. Culpa cum nam praesentium quas sed. Ab amet at commodi, consequuntur culpa ea facilis in itaque labore laboriosam maiores molestias nihil non officia perferendis porro quae similique tempora unde ut velit voluptates voluptatum? Ab accusamus aliquam cupiditate debitis dignissimos, dolor expedita facere id inventore ipsum maxime, molestias optio quasi rem repellat, rerum saepe sequi sunt totam voluptate. Dolorem dolorum illum impedit minima nesciunt qui quidem recusandae repellat ullam voluptatem. Amet culpa distinctio necessitatibus quidem? At, molestiae.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </section>
    <!-- End generic Area -->
@endsection
