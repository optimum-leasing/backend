<!-- Start Header Area -->
<header class="default-header">
    <div class="container-fluid">
        <div class="header-wrap">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="{{ config('app.url') }}"><img src="/img/logo.png" alt=""></a>
                </div>
                <div class="main-menubar d-flex align-items-center">
                    <nav class="hide">
                        <a href="{{ config('app.url') }}">Home</a>
                        <a href="{{ config('app.form_a_url') }}">A event</a>
                        <a href="{{ config('app.form_b_url') }}">B event</a>
                    </nav>
                    <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header Area -->
