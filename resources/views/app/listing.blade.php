@extends('app.layouts.app')

@section('content')
    <section class="about-generic-area">
        <div class="container border-top-generic --page-html-content">
            <h2 class="mb-5">{{ $oListing->title }}</h2>
            <div>
                {!! $oListing->title !!}
            </div>
        </div>
    </section>
@endsection
