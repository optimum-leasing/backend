@extends('app.layouts.app')

@section('content')
    <section class="about-generic-area">
        <div class="container border-top-generic --page-html-content">
            <h2 class="mb-5">{{ $oPage->title }}</h2>
            <div>
                {!! $oPage->text !!}
            </div>
        </div>
    </section>
@endsection
