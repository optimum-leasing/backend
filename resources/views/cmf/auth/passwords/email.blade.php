@extends('admin.layouts.admin')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="columns">
                <div class="column is-half is-offset-one-quarter box" style="padding: 20px 30px 30px 30px;">
                    <h1 class="title">Reset Password</h1>



                    @if (session('status'))
                        <div class="container">
                            <div class="notification is-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif

                    <form role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary">Send Password Reset Link</button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
