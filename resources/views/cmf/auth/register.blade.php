@extends('admin.layouts.auth')

@section('body')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-group mb-0">
                    <form class="card p-2 ajax-form" action="{{ routeCmf('register.post') }}">
                        <input type="hidden" name="adminToken" value="{{ md5(str_random(16)) }}">
                        <div class="card-block">
                            <h1>Регистрация</h1>
                            <p class="text-muted">Добро пожаловать</p>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="text" class="form-control" placeholder="{{ trans('form.name') }}" name="first_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="text" class="form-control" placeholder="{{ trans('form.email') }}" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="{{ trans('form.password') }}" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="{{ trans('form.password_confirm') }}" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-2 inner-form-submit">{{ trans('auth.registration') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card card-inverse card-primary py-3 hidden-md-down" style="width:44%">
                        <div class="card-block text-center" style="display: flex;align-items: center;">
                            <div style="width: 100%;">
                                <h2>{{ $oComposerSite->app->title }}</h2>
                                <p>Панель управления</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section hidden {{-- is-medium --}}" style="padding-top: 0;padding-left: 1.75rem;">
        <div class="container">
            <div class="columns">
                <div class="column is-half @if($sComposerLayout === 'app') is-offset-one-quarter @endif" style="padding: 0;">
                    <form role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">Name</label>
                        <p class="control">
                            <input id="name" type="text" class="input {{ $errors->has('name') ? 'is-danger' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="help is-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </p>

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <label for="password" class="label">Password</label>
                        <p class="control">
                            <input id="password" type="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </p>

                        <label for="password-confirm" class="label">Confirm Password</label>
                        <p class="control">
                            <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary">Register</button>
                            </p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
