{{--@if($sComposerRouteView && Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.count'))--}}
{{--    <div style="position: absolute;top: 46px;right: 35px;" class="--count-table-view font-weight-bold text-black">--}}
{{--        <span class="--get">{{ Session::get($sComposerRouteView.'.count.get') }}</span>&nbsp;/&nbsp;<span class="--all">{{ Session::get($sComposerRouteView.'.count.total') }}</span>--}}
{{--    </div>--}}
{{--@endif--}}
@hasSection('breadcrumb')
    @yield('breadcrumb')
@else
    @if(isset($sComposerRouteView) && !empty($sComposerRouteView) && $sComposerRouteView)
        @if(isset($breadcrumb) && !empty($breadcrumb))
            @if(isset($oItem) && !empty($oItem))
                {{ Breadcrumbs::render($breadcrumb.'_item', $oItem) }}
            @else
                {{ Breadcrumbs::render($breadcrumb) }}
            @endif
        @else
            @if(isset($oItem) && !empty($oItem))
                {{ Breadcrumbs::render($sComposerRouteView, $oItem) }}
            @else
                {{ Breadcrumbs::render($sComposerRouteView, null) }}
            @endif
        @endif
    @else
        {{ Breadcrumbs::render('dashboard') }}
    @endif
@endif
