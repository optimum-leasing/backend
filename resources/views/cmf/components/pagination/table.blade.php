<table class="table">
    <thead>
        <tr>
            <th>Имя</th>
            <th>Email</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Имя</th>
            <th>Email</th>
        </tr>
    </tfoot>
    <tbody>
    @foreach($oUsers as $oUser)
        <tr>
            <td>{{ $oUser->name }}</td>
            <td>{{ $oUser->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oUsers instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oUsers->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oUsers->links('bulma.components.pagination.ajax') }}
    @endif
@endif