@if(Session::has('toastr::notifications'))
    {!! Toastr::render() !!}
@else
    <script>
        window.toastrOptions = @json(config('toastr.options'));
    </script>
@endif
