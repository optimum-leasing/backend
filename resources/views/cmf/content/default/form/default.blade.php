
    @if($field['dataType'] !== App\Cmf\Core\MainController::DATA_TYPE_CHECKBOX && empty($no_title) && (!isset($field['showEdit']) || $field['showEdit']))
        <label>{{ $field['title'] }}{!! !empty($field['required']) ? '<i class="r">*</i>' : '' !!}</label>
    @endif

    @if(View::exists('cmf.content.'.$model.'.form.field.'.$name))
        @include('cmf.content.'.$model.'.form.field.'.$name, [
            'oItem' => $item
        ])
    @else
        @switch($field['dataType'])
            @case(App\Cmf\Core\MainController::DATA_TYPE_NUMBER)
            <input type="text" class="form-control" name="{{ $name }}" placeholder="{{ $field['title'] }}" data-role="js-mask-int" data-length="{{ $field['length'] ?? 2 }}"
                   value="{{ $item->$name ?? $value ?? $field['default'] ?? '' }}" {{ !empty($field['required']) ? 'required' : '' }}>
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_TEXT)
                <input type="text" class="form-control" name="{{ $name }}" placeholder="{{ $field['title'] }}" id="{{ $name }}_id"
                {{ !empty($field['readOnly']) ? 'readonly' : '' }}
                value="{{ $item->$name ?? $field['default'] ?? '' }}" {{ !empty($field['required']) ? 'required' : '' }}
{{--                {{ $name === 'phone' ? 'data-role=js-mask-phone' : '' }}--}}
                >
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_SELECT)
                <select class="form-control selectpicker {{ isset($field['relationship']) ? 'with-ajax' : '' }}" id="{{ $name }}_id"
                        name="{{ $name }}{{ !empty($field['multiple']) ? '[]' : '' }}"
                        {{ !empty($field['required']) ? 'required' : '' }}
                        {{ !empty($field['attributes']) ? implode(' ', $field['attributes']) : '' }}
                        {{ !empty($field['multiple']) ? 'multiple' : '' }}
                        {{ isset($field['relationship']) ? '
                        data-live-search="true"

                        data-abs-ajax-url='.routeCmf($model.'.action.item.post', ['id' => 0, 'name' => 'searchRelationshipField?field=' . $name . '']).'
                        data-selected='.implode(',', $field['selected_values']) : ''}}
                >
                    @if(!empty($field['empty']))
                        <option value="">Ничего не выбрано</option>
                    @endif
                    @foreach($field['values'] as $key => $value)
                        <option value="{{ $key }}"
                            {{ in_array($key, $field['selected_values']) ? 'selected' : '' }}
                        >
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_CHECKBOX)
                <div class="abc-checkbox">
                    <input type="checkbox" id="checkbox-{{ $name }}" class="styled" name="{{ $name }}" value="1" id="{{ $name }}_id"
                            {{ isset($item) && $item->$name ? 'checked' : '' }}
                            {{ !isset($item) && !empty($field['default']) ? 'checked' : '' }}
                            {{ !empty($field['required']) ? 'required' : '' }}
                    >
                    <label for="checkbox-{{ $name }}">{{ $field['title_form'] ?? $field['title'] }}{!! !empty($field['required']) ? '<i class="r">*</i>' : '' !!}</label>
                </div>
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_DATE)
                <div class="input-group">
                    <div class="input-group-prepend bg-default">
                        <span class="input-group-text">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                    <input type="text" id="{{ $name }}_id" class="form-control datetimepicker" name="{{ $name }}"
                        {{ !empty($field['required']) ? 'required' : '' }}
                        @if(isset($field['parent']))
                            data-parent="{{ $field['parent'] }}"
                        @endif
                        @if(isset($field['datetime']) && $field['datetime'])
                            data-format="datetime"
                            data-role="js-mask-datetime"
                            value="{{ isset($item) && !is_null($item->$name) ? $item->$name->format('d.m.Y H:i') : $field['default'] ?? '' }}"
                        @else
                            data-format="date"
                            data-role="js-mask-datetime"
                            value="{{ isset($item) && !is_null($item->$name) ? $item->$name->format('d.m.Y') : $field['default'] ?? '' }}"
                        @endif
                            placeholder="{{ $field['title'] }}"
                    >
                </div>
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA)
                <textarea class="form-control markdown" name="{{ $name }}" cols="15" rows="5" placeholder="{{ $field['title'] }}" id="{{ $name }}_id"
                        {{ !empty($field['required']) ? 'required' : '' }}
                >{{ $item->$name ?? $field['default'] ?? '' }}</textarea>
                <div class="markdown-container">
                    <a class="markdown-toggler btn btn-sm text-black" title="Предпросмотр">
                        <i class="fa fa-eye"></i>
                    </a>
                    <div class="markdown-window hidden">
                        <span class="badge badge-default">Предпросмотр</span>
                        <div class="markdown-preview" id="{{ $name }}_id-markdown-preview"></div>
                    </div>
                </div>
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_IMG)
            @case(App\Cmf\Core\MainController::DATA_TYPE_FILE)
                <input id="{{ $name }}-file-id" type="file" class="form-control" name="{{ $name }}" placeholder="{{ $field['title'] }}"
                value="{{ $item->$name ?? $field['default'] ?? '' }}"
                    {{ !empty($field['required']) ? 'required' : '' }}
                    {{ !empty($field['accept']) ? 'accept=' . $field['accept'] . '': '' }}
                >
            @break
            @case(App\Cmf\Core\MainController::DATA_TYPE_CUSTOM)
                @if(View::exists('cmf.content.'.$model.'.form.field.'.$name))
                    @include('cmf.content.'.$model.'.form.field.'.$name, [
                        'oItem' => $item
                    ])
                @endif
            @break
        @endswitch
    @endif
