<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        @if(isset($title) || isset($oItem->title))
            <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
                <div class="col-12">
                    {{ $title ?? $oItem->title ?? '' }}
                </div>
            </div>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 mb-12">
                @if(is_array($tabs))
                    @foreach($tabs as $tab)
                        @if($loop->first)
                            <ul class="nav nav-tabs" role="tablist">
                        @endif
                        <li class="nav-item {{ isset($tab['hidden']) && $tab['hidden'] ? 'hidden' : '' }}">
                        <a class="nav-link @if($loop->first)active @endif" data-toggle="tab" href="#user-edit-tab-{{ $loop->index }}" role="tab"
                            @foreach($tab['tabs_attributes'] as $attribute => $value)
                                {{ $attribute }} = "{{ $value }}"
                            @endforeach
                        >{{ $tab['title'] }}</a>
                        </li>
                        @if($loop->last)
                            </ul>
                        @endif
                    @endforeach
                @endif
                @if(is_array($tabs))
                    @foreach($tabs as $key => $tab)
                        @if($loop->first)
                            <div class="tab-content">
                        @endif
                        <div class="tab-pane tab-submit @if($loop->first)active @endif" id="user-edit-tab-{{ $loop->index }}" role="tabpanel"
                            @foreach($tab['content_attributes'] as $attribute => $value)
                                {{ $attribute }} = "{{ $value }}"
                            @endforeach
                        >
                            @if(View::exists('cmf.content.' . $sComposerRouteView . '.modals.'.$key))
                                @include('cmf.content.' . $sComposerRouteView . '.modals.'.$key)
                            @else
                                @include('cmf.content.default.modals.'.$key)
                            @endif
                        </div>
                        @if($loop->last)
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="tab-pane tab-submit active">
                        @if(View::exists('cmf.content.' . $sComposerRouteView . '.modals.edit'))
                            @include('cmf.content.' . $sComposerRouteView . '.modals.edit')
                        @else
                            @include('cmf.content.default.modals.edit')
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
