@php
    $model = isset($model) ? $model : $sComposerRouteView;
@endphp
<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Просмотр</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-12">
                @if(View::exists('cmf.content.' . $model . '.modals.show'))
                    @include('cmf.content.' . $model . '.modals.show')
                @else
                    @include('cmf.content.default.modals.show')
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>
