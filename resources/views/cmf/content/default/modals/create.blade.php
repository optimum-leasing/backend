@foreach($fields as $name => $field)
    @if(isset($field['form']) && $field['form'] === false)

    @else
        <div class="form-group is-{{ $name }}-group {{ isset($field['hidden']) && $field['hidden'] ? 'hidden' : '' }}">
            @include('cmf.content.default.form.default', [
                'name' => $name,
                'item' => null,
                'field' => $field,
             ])
        </div>
    @endif
@endforeach
