<form class="row ajax-form"
      id="user-from"
      action="{{ routeCmf($model.'.update', ['id' => $oItem->id]) }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ routeCmf($model.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit, editForm{{ ucfirst($model) }}"
>
    <input type="hidden" name="id" value="{{ $oItem->id }}">
    <div class="col-12">
        <?php
            $aGroups = [];
        ?>
        @foreach($fields as $name => $field)
            @if(isset($field['form']) && $field['form'] === false)

            @else
                @if(isset($field['group']) && !isset($aGroups[$field['group']]))
                    <div class="form-group is-row-{{ $field['group'] }}-group row">
                        <?php
                            $aGroups[$field['group']] = collect($fields)->where('group', $field['group']);
                        ?>
                        @foreach($aGroups[$field['group']] as $groupName => $groupField)
                            <div class="form-group
                                col-{{ $groupField['group-col'] }}
                                is-{{ $groupName }}-group
                                {{ isset($groupField['hidden']) && $groupField['hidden'] ? 'hidden' : '' }}
                            ">
                                @include('cmf.content.default.form.default', [
                                    'name' => $groupName,
                                    'item' => $oItem,
                                    'field' => $groupField
                                ])
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="form-group is-{{ $name }}-group {{ isset($field['hidden']) && $field['hidden'] ? 'hidden' : '' }}">
                        @include('cmf.content.default.form.default', [
                            'name' => $name,
                            'item' => $oItem,
                            'field' => $field
                        ])
                    </div>
                @endif
            @endif
        @endforeach
    </div>
    {{ $slot ?? '' }}
</form>
