<div class="form-group">
    <label class="text-muted m-0">ID:</label>
    <br>
    {{ $oItem->id }}
</div>
@foreach($fields as $field => $value)
    <div class="form-group">
        @include('cmf.content.default.table.column.default', [
            'name' => $field,
            'item' => $oItem,
            'value' => $value
        ])
    </div>
@endforeach
