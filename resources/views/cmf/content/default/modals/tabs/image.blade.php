@php
    $sComposerRouteView = isset($sComposerRouteView) ? $sComposerRouteView : $view;
@endphp

@include('cmf.components.gallery.gallery', [
    'priorityUrl' => routeCmf($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
    'uploadUrl' => routeCmf($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
    'model' => $sComposerRouteView,
    'oItem' => $oItem,
    'form' => '#'.$sComposerRouteView.'-form'
])
