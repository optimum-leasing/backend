@php
    $sComposerRouteView = isset($sComposerRouteView) ? $sComposerRouteView : $view;
@endphp

@include('cmf.components.text.text', [
    'model' => $sComposerRouteView,
    'oItem' => $oItem,
    'form' => '#'.$sComposerRouteView.'-form',
    'itemText' => $oItem->description,
])
