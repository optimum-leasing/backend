@if(method_exists($oItem, 'statuses'))
    <span class="badge {{ $oItem->status_icon['class'] }}">{{ $oItem->status_text }}</span>
@else
    @if($oItem->status === 1)
        <span class="badge badge-success">Активно</span>
    @else
        <span class="badge badge-danger">Не активно</span>
    @endif
@endif
