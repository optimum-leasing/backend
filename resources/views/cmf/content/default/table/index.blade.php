@extends('cmf.layouts.cmf')

@php
    $hasSearch = false;
@endphp
@foreach($fields as $key => $field)
    @if(!is_null(Request()->{$key}))
        @php
            $hasSearch = true;
        @endphp
    @endif
@endforeach

@section('content.title')
    @include('cmf.content.default.table.title')
@endsection
@section('breadcrumb-search')
    @component('cmf.content.default.table.search.bar', ['hasSearch' => $hasSearch])
        <form id="search-bar-form" action="{{ routeCmf($sComposerRouteView.'.query') }}" class="row ajax-form"
              data-loading="1"
              data-view=".admin-table"
              data-callback="updateView"
              data-delay="1"
{{--              data-fast-search=""--}}
              data-outer-loading='a.--search-bar-submit'
        >
            @if(\Illuminate\Support\Facades\View::exists('cmf.content.' . $sComposerRouteView . '.table.search.data'))
                @include('cmf.content.' . $sComposerRouteView . '.table.search.data')
            @else
                @include('cmf.content.default.table.search.data')
            @endif
        </form>
    @endcomponent
@endsection
@section('breadcrumb-right')
    @if(\Illuminate\Support\Facades\View::exists('cmf.content.' . $sComposerRouteView . '.table.create'))
        @include('cmf.content.' . $sComposerRouteView . '.table.create')
    @else
        @include('cmf.content.default.table.create')
    @endif
    <a class="btn btn-primary ajax-link --search-bar-submit {{ $hasSearch ? '' : 'hidden' }}" href="#"
       data-form="#search-bar-form"
       style="margin-right: 10px; padding-left: 1rem;"
    >
        Найти
    </a>
    <a class="btn btn-default text-black" data-toggle="collapse" href="#search-bar" aria-expanded="true" aria-controls="search-bar"
       style="margin-right: 10px; width: 47px;padding-left: 1rem;"
    >
        <i class="fa fa-search" aria-hidden="true"></i>
    </a>
@endsection

@section('content')
    @component('cmf.components.table.index')
        <div class="admin-table table-component-pagination">
            @include('cmf.content.default.table.table_before', [
                'oItems' => $oItems
            ])
        </div>
    @endcomponent
@endsection
