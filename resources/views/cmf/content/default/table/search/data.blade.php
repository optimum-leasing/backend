@if(isset($fields['name']))
    <div class="col-8">
        <div class="form-group">
            <label>Название или ID</label>
            <input class="form-control ajax-input" data-form="#search-bar-form" type="text" placeholder="Название" name="name" value="{{ Request()->name ?? '' }}">
        </div>
    </div>
@endif
@if(isset($fields['promocode']))
    <div class="col-8">
        <div class="form-group">
            <label>{{ $fields['promocode']['title'] }}</label>
            <input class="form-control ajax-input" data-form="#search-bar-form" type="text" placeholder="{{ $fields['promocode']['title'] }}" name="promocode" value="{{ Request()->promocode ?? '' }}">
        </div>
    </div>
@endif
@if(isset($fields['active']))
    <div class="col-4">
        <div class="form-group">
            <label>Активно</label>
            <select class="form-control ajax-select" data-form="#search-bar-form" name="status">
                <option value="">Ничего не выбрано</option>
                <option value="0" {{ Request()->active === '0' ? 'selected' : '' }}>Не активно</option>
                <option value="1" {{ Request()->active === '1' ? 'selected' : '' }}>Активно</option>
            </select>
        </div>
    </div>
@endif
@if(isset($fields['status']['search']))
    <div class="col-4">
        <div class="form-group">
            <label>Статус</label>
            <select class="form-control ajax-select" data-form="#search-bar-form" name="status">
                <option value="">Ничего не выбрано</option>
                <option value="0" {{ Request()->active === '0' ? 'selected' : '' }}>Не активно</option>
                <option value="1" {{ Request()->active === '1' ? 'selected' : '' }}>Активно</option>
            </select>
        </div>
    </div>
@endif
