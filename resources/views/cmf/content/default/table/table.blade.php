<?php
$model = $model ?? $sComposerRouteView;
?>
<table class="table table-admin table--mobile {{ member()->canAdminEdit() ? '__with-edit-delete' : '' }} table-hover">
    <thead>
    <tr>
        <th class="is-id">#</th>
        @if(in_array($model, [
        \App\Cmf\Project\User\UserController::NAME,
        \App\Cmf\Project\Listing\ListingController::NAME,
        ]))
            <th class="is-image"></th>
        @endif
        @foreach($fields as $name => $field)
            @if (!empty($field['in_table']))
                <th class="is-{{ $name }}">{{ $field['table_title'] ?? $field['title'] }}</th>
                @if (!empty($field['delete_title']))
                    @php
                        $delete_title = $field['delete_title'];
                        $delete_title_name = $name;
                    @endphp
                @endif
            @endif
        @endforeach
        @if(member()->canAdminEdit())
            <th><i class="icon-pencil" data-tippy-popover data-tippy-content="Изменить"></i></th>
        @endif
        @if(member()->canAdminDelete())
            <th><i class="icon-trash icons" data-tippy-popover data-tippy-content="Удалить"></i></th>
        @endif
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        @if(in_array($model, [
        \App\Cmf\Project\User\UserController::NAME,
        \App\Cmf\Project\Listing\ListingController::NAME,
        ]))
            <th class="is-image"></th>
        @endif
        @foreach($fields as $name => $field)
            @if (!empty($field['in_table']))
                <th class="is-{{ $name }}">{{ $field['table_title'] ?? $field['title'] }}</th>
            @endif
        @endforeach
        @if(member()->canAdminEdit())
            <th><i class="icon-pencil" data-tippy-popover data-tippy-content="Изменить"></i></th>
        @endif
        @if(member()->canAdminDelete())
            <th><i class="icon-trash icons" data-tippy-popover data-tippy-content="Удалить"></i></th>
        @endif
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(!(method_exists($oItem, 'isActive') ? $oItem->isActive() : $oItem->active)) class="text-opacity" @endif>
            <td>{{ $oItem->id }}</td>
            @if(in_array($model, [
            \App\Cmf\Project\User\UserController::NAME,
            \App\Cmf\Project\Listing\ListingController::NAME,
            ]))
                <td>
                    @if(\Illuminate\Support\Facades\View::exists('cmf.content.' . $model . '.table.column.image'))
                        @include('cmf.content.' . $model . '.table.column.image', [
                            'oItem' => $oItem,
                        ])
                    @else
                        @include('cmf.content.default.table.column.image', [
                            'oItem' => $oItem,
                            'path' => 'square'
                        ])
                    @endif
                </td>
            @endif
            @foreach($fields as $name => $field)
                @if (!empty($field['in_table']))
                    <td class="is-{{ $name }}">
{{--                        @if($loop->first)--}}
{{--                            @include('cmf.content.default.table.column.show', [--}}
{{--                                'oItem' => $oItem,--}}
{{--                                'init'  => ''--}}
{{--                            ])--}}
{{--                        @endif--}}

                        @if(\Illuminate\Support\Facades\View::exists('cmf.content.' . $model . '.table.column.' . $name))
                            @include('cmf.content.' . $model . '.table.column.' . $name, [
                                'name' => $name,
                                'item' => $oItem,
                                'value' => $field,
                                'hide_title' => true,
                            ])
                        @else
                            @include('cmf.content.default.table.column.default', [
                                'name' => $name,
                                'item' => $oItem,
                                'value' => $field,
                                'hide_title' => true,
                            ])
                        @endif

                    </td>
                @endif
            @endforeach
            @if(member()->canAdminEdit())
                <td>
                    @if(\Illuminate\Support\Facades\View::exists('cmf.content.' . $model . '.table.column.edit'))
                        @include('cmf.content.' . $model . '.table.column.edit', [
                            'oItem' => $oItem,
                        ])
                    @else
                        @include('cmf.content.default.table.column.edit', [
                            'oItem' => $oItem,
                            'init' => 'uploader, initSummernote'
                        ])
                    @endif
                </td>
            @endif
            @if(member()->canAdminDelete())
                <td>
                    @include('cmf.content.default.table.column.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => $delete_title ?? $model,
                        'deleteValue' => isset($delete_title_name) ? $oItem->$delete_title_name : $model,
                    ])
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
@include('cmf.content.default.table.pagination', [
    'oItems' => $oItems
])
