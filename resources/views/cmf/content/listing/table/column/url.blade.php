<a href="{{ $oItem->url }}" target="_blank" data-tippy-popover data-tippy-content="{{ $oItem->url }}">
    {{ \Illuminate\Support\Str::limit(str_replace(config('app.url'), '', $oItem->url), 30) }}
</a>


