<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Системные настройки</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#user-commands-{{ $sComposerRouteView }}-1" role="tab" aria-controls="user-commands-1" aria-expanded="true"
                       data-hidden-submit="1"
                    >
                        Консольные команды
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="user-commands-{{ $sComposerRouteView }}-1" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="hr-label">
                            <label>Кэш</label>
                            <hr>
                        </div>
                        <div class="col-12">
                            <a class="btn btn-danger" href="{{ routeCmf('dev.command.index', ['name' => 'cache:clear']) }}">Очистить</a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="hr-label">
                            <label>
                                Шаблоны
                                {{--<i class="fa fa-circle-o"></i>--}}
                            </label>
                            <hr>
                        </div>
                        <div class="col-12">
                            <a class="btn btn-success" href="{{ routeCmf('dev.command.index', ['name' => 'view:cache']) }}">Закешировать</a>
                            <a class="btn btn-danger" href="{{ routeCmf('dev.command.index', ['name' => 'view:clear']) }}">Очистить</a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="hr-label">
                            <label>
                                Конфигурация
                                {{--<i class="fa fa-cog"></i>--}}
                            </label>
                            <hr>
                        </div>
                        <div class="col-12">
                            <a class="btn btn-success" href="{{ routeCmf('dev.command.index', ['name' => 'config:cache']) }}">Закешировать</a>
                            <a class="btn btn-danger" href="{{ routeCmf('dev.command.index', ['name' => 'config:clear']) }}">Очистить</a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="hr-label">
                            <label>
                                Роуты
                                {{--<i class="fa fa-cog"></i>--}}
                            </label>
                            <hr>
                        </div>
                        <div class="col-12">
                            <a class="btn btn-success" href="{{ routeCmf('dev.command.index', ['name' => 'route:cache']) }}">Закешировать</a>
                            <a class="btn btn-danger" href="{{ routeCmf('dev.command.index', ['name' => 'route:clear']) }}">Очистить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link hidden" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
