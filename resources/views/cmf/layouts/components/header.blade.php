<header class="app-header navbar">
{{--    <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>--}}
    <a class="navbar-brand" href="{{ config('app.url') }}" style="background-size: 140px auto;background: #141519;color: #fff;text-align: center;font-size: 16px;line-height: 33px;">
        {{ $oComposerSite->app->title_short ?? 'app.title_short' }}
    </a>

    <ul class="nav navbar-nav hidden-md-down">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler"
               data-url="{{ routeCmf('user.action.post', ['name' => 'saveSidebarToggle']) }}"
               href="#"
            >☰</a>
        </li>
        {{--
        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/dashboard') }}">
                @if(isset(config('project.menu.admin.dashboard')['title']))
                    {{ config('project.menu.admin.dashboard')['title']}}
                @else
                    Dashboard
                @endif
            </a>
        </li>
        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/users') }}">
                @if(isset(config('project.menu.admin.users')['title']))
                    {{ config('project.menu.admin.users')['title']}}
                @else
                    Users
                @endif
            </a>
        </li>
        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/settings') }}">
                @if(isset(config('project.menu.admin.settings')['title']))
                    {{ config('project.menu.admin.settings')['title']}}
                @else
                    Settings
                @endif
            </a>
        </li>
        --}}
    </ul>

    <ul class="nav navbar-nav ml-auto">
        {{--<li class="nav-item dropdown hidden-md-down">--}}
            {{--<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--}}
                {{--<i class="icon-share-alt"></i>--}}
                {{--<span class="badge badge-pill badge-danger">{{ count($oComposerInvoices)}}</span>--}}
            {{--</a>--}}
            {{--<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">--}}

                {{--<div class="dropdown-header text-center">--}}
                    {{--<strong>У вас {{ count($oComposerInvoices) }} новых заказов</strong>--}}
                {{--</div>--}}
                {{--<a href="{{ route('invoice.index') }}" class="dropdown-item">--}}
                    {{--<i class="icon-user-follow text-success"></i> Перейти к ним--}}
                {{--</a>--}}
                {{--<div class="dropdown-header text-center">--}}
                    {{--<strong>Статистика</strong>--}}
                {{--</div>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-user-unfollow text-danger"></i> Всего <span style="float: right;">{{ count($oComposerInvoices) }}</span>--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-chart text-info"></i> Закрытых <span style="float: right;">{{ count($oComposerInvoices->where('closed_at', '<>', null)) }}</span>--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-basket-loaded text-primary"></i> Не закрытых <span style="float: right;">{{ count($oComposerInvoices->where('closed_at', null)) }}</span>--}}
                {{--</a>--}}
                {{----}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-speedometer text-warning"></i> Общая сумма на не закрытых <span style="float: right;">{{ $oComposerInvoices->where('closed_at', '<>', null)->sum('amount') }}</span>--}}
                {{--</a>--}}


                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-user-unfollow text-danger"></i> User deleted--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-chart text-info"></i> Sales report is ready--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-basket-loaded text-primary"></i> New client--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<i class="icon-speedometer text-warning"></i> Server overloaded--}}
                {{--</a>--}}
                {{----}}
                {{----}}
                {{--<div class="dropdown-header text-center">--}}
                    {{--<strong>Server</strong>--}}
                {{--</div>--}}

                {{--<a href="#" class="dropdown-item">--}}
                    {{--<div class="text-uppercase mb-q">--}}
                        {{--<small><b>CPU Usage</b>--}}
                        {{--</small>--}}
                    {{--</div>--}}
                    {{--<span class="progress progress-xs">--}}
                            {{--<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>--}}
                        {{--</span>--}}
                    {{--<small class="text-muted">348 Processes. 1/4 Cores.</small>--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<div class="text-uppercase mb-q">--}}
                        {{--<small><b>Memory Usage</b>--}}
                        {{--</small>--}}
                    {{--</div>--}}
                    {{--<span class="progress progress-xs">--}}
                            {{--<div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
                        {{--</span>--}}
                    {{--<small class="text-muted">11444GB/16384MB</small>--}}
                {{--</a>--}}
                {{--<a href="#" class="dropdown-item">--}}
                    {{--<div class="text-uppercase mb-q">--}}
                        {{--<small><b>SSD 1 Usage</b>--}}
                        {{--</small>--}}
                    {{--</div>--}}
                    {{--<span class="progress progress-xs">--}}
                            {{--<div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>--}}
                        {{--</span>--}}
                    {{--<small class="text-muted">243GB/256GB</small>--}}
                {{--</a>--}}
                {{----}}
            {{--</div>--}}
        {{--</li>--}}
        {{--
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 5 notifications</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <i class="icon-user-follow text-success"></i> New user registered
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-unfollow text-danger"></i> User deleted
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-chart text-info"></i> Sales report is ready
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-basket-loaded text-primary"></i> New client
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-speedometer text-warning"></i> Server overloaded
                </a>

                <div class="dropdown-header text-center">
                    <strong>Server</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>CPU Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>Memory Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">11444GB/16384MB</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>SSD 1 Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">243GB/256GB</small>
                </a>

            </div>
        </li>
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-list"></i>
                <span class="badge badge-pill badge-warning">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 5 pending tasks</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Upgrade NPM &amp; Bower
                            <span class="float-right">
                                <strong>0%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">ReactJS Version
                            <span class="float-right">
                                <strong>25%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">VueJS Version
                            <span class="float-right">
                                <strong>50%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Add new layouts
                            <span class="float-right">
                                <strong>75%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Angular 2 Cli Version
                            <span class="float-right">
                                <strong>100%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>

                <a href="#" class="dropdown-item text-center">
                    <strong>View all tasks</strong>
                </a>
            </div>
        </li>
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-envelope-letter"></i>
                <span class="badge badge-pill badge-info">7</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 4 messages</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/7.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-success"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">Just now</small>
                        </div>
                        <div class="text-truncate font-weight-bold">
                            <span class="fa fa-exclamation text-danger"></span>Important message</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/6.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-warning"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">5 minutes ago</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/5.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-danger"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">1:52 PM</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/4.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-info"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">4:03 PM</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>

                <a href="#" class="dropdown-item text-center">
                    <strong>View all messages</strong>
                </a>
            </div>
        </li>
        <li class="nav-item hidden-md-down">
            <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
        </li>
        --}}
        <li class="nav-item hidden-md-down">
            <a class="nav-link text-success" href="#"
               data-tippy-popover
               data-tippy-content="Количество заявок"
            >
                <i class="fa fa-phone" aria-hidden="true"></i>
                @if(isset($oComposerCalls) && $oComposerCalls->count() !== 0)
                    <span class="badge badge-pill badge-success">{{ $oComposerCalls->count() }}</span>
                @endif
            </a>
        </li>
{{--        <li class="nav-item hidden-md-down">--}}
{{--            <a class="nav-link" href="#">--}}
{{--                <i class="icon-phone"></i>--}}
{{--            </a>--}}
{{--        </li>--}}
        {{--<li class="nav-item hidden-md-down">--}}
            {{--<a class="nav-link" href="#">--}}
                {{--<i class="icon-location-pin"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--@if($oComposerCommentsNotApproved->count() !== 0)--}}
            {{--<li class="nav-item hidden-md-down">--}}
                {{--<a class="nav-link" href="{{ route('comment.index', ['approved' => 0]) }}"--}}
                   {{--data-tippy-popover--}}
                   {{--data-tippy-content="Количество неодобренных комментариев"--}}
                {{-->--}}
                    {{--<i class="icon-bubble"></i>--}}
                    {{--<span class="badge badge-pill badge-danger">{{ $oComposerCommentsNotApproved->count() ?? 0 }}</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--@endif--}}
        {{--<li class="nav-item hidden-md-down">--}}
            {{--<a class="nav-link" href="{{ route('subscription.index') }}"--}}
               {{--data-tippy-popover--}}
               {{--data-tippy-content="Количество подписок"--}}
            {{-->--}}
                {{--<i class="icon-envelope"></i>--}}
                {{--<span class="badge badge-pill badge-success">{{ $oComposerSubscriptions->count() ?? 0 }}</span>--}}
            {{--</a>--}}
        {{--</li>--}}

        <li class="nav-item dropdown" style="padding-right: 20px;">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ $oComposerMember->get('user.image') }}"
                     data-user-image
                     class="img-avatar"
                     alt="{{ $oComposerMember->get('user.email') }}"
                >
                <span class="hidden-md-down" data-user-name>{{ $oComposerMember->get('user.name') }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item trigger"
                   href="#"
                   data-ajax
                   data-dialog="#custom-edit-modal"
                   data-action="{{ routeCmf('user.edit.modal.post', ['id' => $oComposerMember->get('user.id')]) }}"
                   data-ajax-init="uploader"
                >
                    <i class="fa fa-pencil"></i>
                    Редактировать профиль
                </a>
                @if($oComposerMember->get('user.role.name') === 'super-admin')
                    <a class="dropdown-item trigger"
                       href="#"
                       data-ajax
                       data-dialog="#custom-edit-modal"
                       data-action="{{ routeCmf('user.action.post', ['name' => 'getModalCommand']) }}"
                    >
                        <i class="fa fa-cog"></i>
                        Системные настройки
                    </a>
                @endif
                <a class="dropdown-item ajax-link" href="#" action="{{ routeCmf('logout.post') }}">
                    <i class="fa fa-lock"></i>
                    Выйти
                </a>
                {{--
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>

                <a class="dropdown-item" href="#"><i class="fa fa-bell-o"></i> Updates<span class="badge badge-info">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-envelope-o"></i> Messages<span class="badge badge-success">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-tasks"></i> Tasks<span class="badge badge-danger">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-comments"></i> Comments<span class="badge badge-warning">42</span></a>

                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>

                <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i> Settings</a>
                <a class="dropdown-item" href="#"><i class="fa fa-usd"></i> Payments<span class="badge badge-default">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-file"></i> Projects<span class="badge badge-primary">42</span></a>
                <div class="divider"></div>
                <a class="dropdown-item" href="#"><i class="fa fa-shield"></i> Lock Account</a>
                --}}

            </div>
        </li>
        {{--
        <li class="nav-item hidden-md-down">
            <a class="nav-link navbar-toggler aside-menu-toggler" href="#">☰</a>
        </li>
        --}}

    </ul>
</header>
