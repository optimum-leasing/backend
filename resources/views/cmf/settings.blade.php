<form class="modal-content dialog__content ajax-form"
      action="{{ routeCmf('dev.settings.save') }}"
      data-callback="refreshAfterSubmit"
>
    <div class="row --connections-inputs">
        <div class="col-12">
            @foreach($settings as $block => $keys)
                @foreach($keys as $key => $value)
                    <div class="form-group --connection-block" data-connection="{{ $key !== 'DB_CONNECTION' ? $block : 'DB_CONNECTION'}}">

                        @if($block === 'db' && $key === 'DB_CONNECTION')
                            <label>Подключение через</label>
                            <select class="form-control selectpicker" name="{{ $block }}[{{ $key }}]"
                                    data-callback="systemChangeConnection"
                                    data-change="1"
                            >
                                <option value="mysql" {{ $value === 'mysql' ? 'selected' : '' }} data-block="db">TCP/IP</option>
                                <option value="mysql_tunnel" {{ $value === 'mysql_tunnel' ? 'selected' : '' }} data-block="tunneler">SSH тунель
                                </option>
                            </select>
                            <hr>
                            @continue
                        @endif
                        <label>{{ $key }}</label>
                        <input type="text" class="form-control" name="{{ $block }}[{{ $key }}]" value="{{ $value }}">
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
</form>
