<?php

use \Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'domain' => cmfHelper('auth')->getUrl(),
    'as' => cmfHelper('auth')->getAs(),
    'prefix' => cmfHelper('auth')->getPrefix(),
    'middleware' => ['guest'],
], function () {
    Route::get('/login', ['uses' => '\\' . LoginController::class . '@showAdminLoginForm', 'as' => 'login']);
    Route::post('/login', ['uses' => '\\' . LoginController::class . '@login', 'as' => 'login.post']);

    //Route::get('/register', ['uses' => 'Auth\RegisterController@showRegistrationForm', 'as' => 'register']);
    //Route::post('/register', ['uses' => '\\' . RegisterController::class . '@register', 'as' => 'register.post']);

    //Route::get('/password/reset', ['uses' => '\\' . ForgotPasswordController::class . '@showLinkRequestForm', 'as' => 'password.request']);
    //Route::post('/password/email', ['uses' => '\\' . ForgotPasswordController::class . '@sendResetLinkEmail', 'as' => 'password.email.post']);
    //Route::get('/password/reset/{token}', ['uses' => '\\' . ResetPasswordController::class . '@showResetForm', 'as' => 'password.reset']);
    //Route::post('/password/reset', ['uses' => '\\' . ResetPasswordController::class . '@reset', 'as' => 'password.reset.post']);

    //Route::get('/activate', ['uses' => '\\' . LoginController::class . '@showActivateForm', 'as' => 'activate']);
    //Route::post('/activate', ['uses' => '\\' . LoginController::class . '@activate', 'as' => 'activate.post']);

});
