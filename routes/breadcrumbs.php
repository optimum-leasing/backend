<?php

use App\Cmf\Project\User\UserController;
use App\Cmf\Project\Listing\ListingController;
use App\Cmf\Project\Page\PageController;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use App\Cmf\Project\Call\CallController;

/**
 * --------------------------------------------------
 * APP
 * --------------------------------------------------
 */
Breadcrumbs::register('app.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('Главная'), route('index'));
});
/**
 * --------------------------------------------------
 * ADMIN
 * --------------------------------------------------
 */
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('Главная'), routeCmf('dashboard.index'));
});
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', routeCmf('dashboard.index'));
});
Breadcrumbs::register(UserController::NAME, function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(UserController::TITLE, routeCmf(UserController::NAME.'.index'));
});
Breadcrumbs::register(PageController::NAME, function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(PageController::TITLE, routeCmf(PageController::NAME.'.index'));
});
Breadcrumbs::register(ListingController::NAME, function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(ListingController::TITLE, routeCmf(ListingController::NAME.'.index'));
});
Breadcrumbs::register(CallController::NAME, function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(CallController::TITLE, routeCmf(CallController::NAME.'.index'));
});
