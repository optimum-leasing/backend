<?php


/*
|--------------------------------------------------------------------------
| Cmf Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Cmf\Core\RouteCmf;
use App\Cmf\Project\User\UserController;
use App\Cmf\Project\Listing\ListingController;
use App\Cmf\Project\Page\PageController;
use App\Cmf\Project\Call\CallController;
use App\Cmf\Project\HomeController;
use App\Http\Controllers\Auth\LoginController;

Route::group([
    'domain' => cmfHelper()->getUrl(),
    'as' => cmfHelper()->getAs(),
    'prefix' => cmfHelper()->getPrefix(),
    'middleware' => ['cmf', 'member'],
], function () {

    Route::get('/', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'index']);
    Route::get('/home', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'home.index']);
    Route::get('/dashboard', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'dashboard.index']);

    Route::post('/logout', ['uses' => '\\' . LoginController::class . '@logoutAdmin', 'as' => 'logout.post']);

    RouteCmf::resource(UserController::NAME);
    RouteCmf::resource(PageController::NAME);
    RouteCmf::resource(ListingController::NAME);
    RouteCmf::resource(CallController::NAME);

    Route::get('/{name}', ['uses' => '\\' . HomeController::class . '@unknown', 'as' => 'unknown']);
});
