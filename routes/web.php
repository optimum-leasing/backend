<?php

use \Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'domain' => config('app.url'),
], function () {
    Route::get('/', ['uses' => '\\' . IndexController::class . '@index', 'as' => 'index']);
    //Route::get('/form-a', ['uses' => '\\' . IndexController::class . '@formA', 'as' => 'formA']);
    //Route::get('/form-b', ['uses' => '\\' . IndexController::class . '@formB', 'as' => 'formB']);

    Route::get('/listing/{id}/{slug}', ['uses' => '\\' . IndexController::class . '@listingShow', 'as' => 'listing.show']);
    Route::get('/{name}', ['uses' => '\\' . IndexController::class . '@page', 'as' => 'page']);
});
