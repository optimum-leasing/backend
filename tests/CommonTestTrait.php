<?php
/**
 * Copyright (c) 2018-2019 "ИД Панорама"
 * Автор модуля: Дмитрий Поскачей (dposkachei@gmail.com)
 */
namespace Tests;

use App\Models\User;
use Illuminate\Http\Request;
use finfo;
use Illuminate\Http\UploadedFile;

trait CommonTestTrait
{
    /**
     * @var User
     */
    protected $oUser;

    /**
     * @param array $data
     * @param bool $ajax
     * @return Request
     */
    protected function request(array $data = [], bool $ajax = false): Request
    {
        $request = new Request();
        $request->merge($data);
        $request->merge([
            'phpunit' => true,
        ]);

        if ($ajax) {
            $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        }

        return $request;
    }

    /**
     * @param string $url
     * @param array $data
     * @return mixed
     */
    protected function postAjax(string $url, array $data = [])
    {
        return $this->post($url, $data, ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
    }

    /**
     * @param string $fileName
     * @param string $fileDir
     * @return UploadedFile
     */
    protected function getFile(string $fileName, string $fileDir): UploadedFile
    {
        $filename = $fileName;
        $file_path = $fileDir . '/' . $fileName;
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $file = new UploadedFile($file_path, $filename, $finfo->file($file_path), filesize($file_path));
        return $file;
    }

    /**
     * @param string $name
     * @param string $route
     * @param bool $factory
     * @param string $type
     * @param array $parameters
     * @return \Illuminate\Foundation\Testing\TestResponse|null
     */
    public function getResponse(
        string $name,
        string $route,
        bool $factory = false,
        string $type = 'get',
        array $parameters = []
    ) {
        if ($factory) {
            $methodFactory = 'factory' . studly_case($name);
            if (method_exists($this, $methodFactory)) {
                $oItem = $this->{$methodFactory}();
                $parameters = array_merge($parameters, [
                    'id' => $oItem->id,
                ]);
            } else {
                $model = studly_case($name)::inRandomOrder()->first();
                $parameters = array_merge($parameters, [
                    'id' => $model->id,
                ]);
            }
        }
        $url = routeCmf($name . '.' . $route, $parameters);
        $_SERVER['REQUEST_URI'] = $url;
        $response = $type === 'post' ? $this->json(studly_case($type), $url, $parameters) : $this->get($url);
        return $response;
    }

    /**
     * @param string $key
     * @param string $route
     * @param \Illuminate\Foundation\Testing\TestResponse|null $response
     * @param array $statuses 422 может быть только ошибка валидации, для успешного достижения метода этого достаточно
     */
    public function assertResponse($key, $route, ?\Illuminate\Foundation\Testing\TestResponse $response, array $statuses = [])
    {
        $statuses = array_merge($statuses, [200, 422, 404]);
        if (!is_null($response)) {
            $this->assertTrue(
                in_array($response->getStatusCode(), $statuses),
                $this->textRed('Error route ' . $key . '.' . $route . '. With status ' . $response->getStatusCode() . '.')
            );
        }
    }
}
