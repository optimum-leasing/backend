<?php

namespace Tests;

use App\Models\User;

trait FactoryTrait
{
    /**
     * @param string $class
     * @param array $data
     * @return mixed
     */
    protected function factoryMake(string $class, array $data = [])
    {
        return factory($class)->make($data);
    }

    /**
     * @param string $class
     * @param array $data
     * @return array
     */
    protected function factoryRaw(string $class, array $data = []): array
    {
        return array_merge(factory($class)->raw(), $data);
    }

    /**
     * @param array $data
     * @return User
     */
    protected function factoryUser(array $data = []): User
    {
        return factory(User::class)->create($data);
    }

    /**
     * @param array $data
     * @return User
     */
    protected function factoryAdmin(array $data = []): User
    {
        return factory(User::class)->create($data);//->assignRole(User::ROLE_SUPERADMIN);
    }
}
