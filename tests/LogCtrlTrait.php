<?php

namespace Tests;

use Tests\LogMock;
use Illuminate\Support\Facades\Log;

trait LogCtrlTrait
{
    protected $tmpApp;

    public function logOff()
    {
        $this->tmpApp = Log::getFacadeRoot();
        Log::swap(new LogMock());
    }

    public function logOn()
    {
        Log::swap($this->tmpApp);
    }
}
