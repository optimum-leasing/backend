<?php
/**
 * @copyright
 * @author
 */

namespace Tests;

/**
 * Сервисные методы
 */
trait StylesTestTrait
{
    /**
     * Вывод в консоли красным цветом
     *
     * @param string $message
     * @return string
     */
    public function textRed(string $message): string
    {
        return "\033[97;41m" . $message . "\033[0;39m";
    }
}
