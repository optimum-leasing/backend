<?php

namespace Tests\Unit\Controllers\Cmf\Core;

use App\Cmf\Core\MainController;
use App\Cmf\Project\Article\ArticleController;
use App\Cmf\Project\User\UserController;
use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\View;
use Tests\FactoryTrait;
use Tests\TestCase;

class MainControllerTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * Создание сущностей
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Берется любой контроллер
     *
     * @return UserController
     */
    private function controller(): UserController
    {
        return new UserController();
    }

    /**
     * @return string
     */
    private function model(): string
    {
        return User::class;
    }

    /**
     * @return User
     */
    private function factory(): User
    {
        return $this->factoryUser();
    }

    /**
     * @see MainController::index()
     */
    public function testIndex()
    {
        $response = $this->controller()->index($this->request());
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    /**
     * @see MainController::create()
     */
    public function testCreate()
    {
        $response = $this->controller()->create();
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    /**
     * @see MainController::store()
     */
    public function testStore()
    {
        $data = $this->factoryRaw($this->model());
        $data['password_confirmation'] = $data['password'];
        $request = $this->request($data);
        $response = (array)$this->controller()->store($request);
        $this->assertTrue($response['success']);
    }

    /**
     * @see MainController::update()
     */
    public function testUpdate()
    {
        $oItem = $this->factory();
        $data = $this->factoryRaw($this->model());
        unset($data['password']);
        //$data['password_confirmation'] = $data['password'];
        $request = $this->request($data);
        $response = (array)$this->controller()->update($request, $oItem->id);
        $this->assertTrue($response['success']);
    }

    /**
     * @see MainController::show()
     */
    public function testShow()
    {
        $response = $this->controller()->show($this->factory()->id);
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    /**
     * @see MainController::edit()
     */
    public function testEdit()
    {
        $response = $this->controller()->edit($this->factory()->id);
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    /**
     * @see MainController::destroy()
     */
    public function testDestroy()
    {
        $oItem = $this->factory();
        $response = $this->controller()->destroy($oItem->id);
        $this->assertTrue($response['success']);

        $this->assertNull($this->model()::find($oItem->id));
    }

    /**
     * @see MainController::paginate()
     */
    public function testPaginate()
    {
        $oController = $this->controller();
        $oController->setOrderBy([
            'column' => 'name',
            'type' => 'desc',
        ]);
        $response = $oController->paginate($this->model());
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $response);

        $oItem = $this->factory();
        $oController = $this->controller();
        $oController->setQuery([
            'status' => $oItem->status,
        ]);
        $response = $oController->paginate($this->model());
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $response);
    }

    /**
     * @see MainController::status()
     */
    public function testStatus()
    {
        $request = $this->request([
            'status' => 1,
        ]);
        $response = $this->controller()->status($request, $this->factory()->id);
        $this->assertTrue($response['success']);
    }

    /**
     * @see MainController::view()
     *
     * @throws \Throwable
     */
    public function testView()
    {
        $oItem = $this->factory();
        $request = $this->request([
            'name' => $oItem->name,
            'status' => 1,
            'page' => 1, // чтобы удалить из query в дальнейшем, т.е. page может встречаться
        ]);
        $response = $this->controller()->view($request);
        $this->assertTrue($response['success']);
    }
}
