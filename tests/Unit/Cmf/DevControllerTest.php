<?php

namespace Tests\Unit\Controllers\Cmf;

use App\Cmf\Project\DevController;
use Tests\TestCase;

class DevControllerTest extends TestCase
{
    /**
     * @return DevController
     */
    private function controller(): DevController
    {
        return new DevController();
    }

    /**
     * @see DevController::php()
     */
    public function testPhp()
    {
        $response = $this->controller()->php($this->request(), 'info');
        $this->assertTrue($response instanceof \Illuminate\View\View);
    }

    /**
     * @see DevController::command()
     */
    public function testCommand()
    {
        $response = $this->controller()->command($this->request(), 'cache:clear');
        $this->assertTrue($response->getStatusCode() === 302);
    }

    /**
     * @see DevController::command()
     */
    public function testWrongCommand()
    {
        $response = $this->controller()->command($this->request(), 'cache-wrong:clear');
        $this->assertTrue($response->getStatusCode() === 302);
    }
}
