<?php


namespace Tests\Unit\Controllers\Cmf\Project;

use App\Cmf\Project\User\UserController;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * Создание сущностей
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return UserController
     */
    private function controller(): UserController
    {
        return new UserController();
    }

    /**
     * @see UserController::index()
     */
    public function testIndex()
    {
        $response = $this->controller()->index($this->request());
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    /**
     * @see UserController::store()
     */
    public function testStore()
    {
        $data = $this->factoryRaw(User::class);
        $data = array_merge($data, [
            'password_confirmation' => $data['password'],
        ]);
        $request = $this->request($data);
        $response = (array)$this->controller()->store($request);
        $this->assertTrue($response['success']);
    }

    /**
     * @see UserController::update()
     */
    public function testUpdate()
    {
        $oItem = $this->factoryUser();
        $oldEmail = $oItem->email;
        $data = $this->factoryRaw(User::class);
        unset($data['password']);

        $request = $this->request($data);
        $response = (array)$this->controller()->update($request, $oItem->id);
        $this->assertTrue($response['success']);

        $oUser = User::find($oItem->id);

        $this->assertTrue($oUser->email !== $oldEmail);
        $this->assertTrue($oUser->email === $data['email']);
    }

    /**
     * @see UserController::update()
     */
    public function testUpdateExistsEmail()
    {
        $oItem = $this->factoryUser();
        $data = $this->factoryRaw(User::class);
        unset($data['password']);

        // существующий email
        $oSecondItem = $this->factoryUser();
        $data['email'] = $oSecondItem->email;

        $request = $this->request($data);
        $response = $this->controller()->update($request, $oItem->id);

        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response);
        $this->assertTrue($response->getStatusCode() === 422);
    }

    /**
     * @see UserController::update()
     */
    public function testUpdatePassword()
    {
        // создание пользователя с нужным паролем
        $data = $this->factoryRaw(User::class);
        $data['password'] = '1234567890';
        $data = array_merge($data, [
            'password_confirmation' => $data['password'],
        ]);
        $request = $this->request($data);
        $response = (array)$this->controller()->store($request);
        $this->assertTrue($response['success']);

        $oItem = User::where('email', $data['email'])->first();
        $data = $this->factoryRaw(User::class);
        unset($data['name']);
        unset($data['login']);
        unset($data['email']);
        $data['password'] = '1234567890';
        $data = array_merge($data, [
            'password_confirmation' => $data['password'],
        ]);

        // неверный пароль
        $data['old_password'] = '12345678901';
        $request = $this->request($data);
        $response = $this->controller()->update($request, $oItem->id);

        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response);
        $this->assertTrue($response->getStatusCode() === 422);

        // верный пароль
        $data['old_password'] = '1234567890';
        $request = $this->request($data);
        $response = (array)$this->controller()->update($request, $oItem->id);

        $this->assertTrue($response['success']);
    }

    /**
     * @see UserController::destroy()
     */
    public function testDestroy()
    {
        $oItem = $this->factoryUser();

        $response = $this->controller()->destroy($oItem->id);
        $this->assertTrue($response['success']);
    }

    /**
     * @see UserController::saveSidebarToggle()
     */
    public function testAction()
    {
        $response = $this->controller()->action($this->request([
            'toggle' => 'true',
        ]), 'saveSidebarToggle');
        $this->assertTrue($response['success']);
        $this->assertTrue(session()->has('sidebar-toggle'));

        try {
            $response = $this->controller()->action($this->request([
                'toggle' => 'true',
            ]), 'wrongMethod');
        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            $this->assertEquals(500, $e->getStatusCode());
        }
    }

    /**
     * @see UserController::saveSidebarToggle()
     */
    public function testSaveSidebarToggle()
    {
        $response = $this->controller()->saveSidebarToggle($this->request([
            'toggle' => 'true',
        ]));
        $this->assertTrue($response['success']);
        $this->assertTrue(session()->has('sidebar-toggle'));

        $response = $this->controller()->saveSidebarToggle($this->request());
        $this->assertTrue($response['success']);
        $this->assertTrue(!session()->has('sidebar-toggle'));
    }

    /**
     * @see UserController::getModalCommand()
     * @throws \Throwable
     */
    public function testGetModalCommand()
    {
        $response = $this->controller()->getModalCommand($this->request());
        $this->assertTrue($response['success']);
    }

    /**
     * @see ArticleController::imageUpload()
     *
     * @throws \Throwable
     */
    public function testImageUpload()
    {
        $oItem = $this->factoryAdmin();

        // вход под админом чтобы отсделить изменение его аватарки
        $this->actingAs($oItem);

        // когда вообще ничего не передается
        $oItem = $this->factoryUser();
        $response = $this->controller()->imageUpload($this->request(), $oItem->id);
        $this->assertEquals(422, $response->getStatusCode());

        // передаются изображения с принудительной валидацией
        $response = $this->controller()->imageUpload($this->request([
            'validate' => true,
            'path-images' => [
                public_path('img/default/user.png'),
            ],
        ]), $oItem->id);
        $this->assertEquals(422, $response->getStatusCode());

        // передаются изображения директорией
        $response = $this->controller()->imageUpload($this->request([
            'path-images' => [
                public_path('img/default/user.png'),
            ],
        ]), $oItem->id);
        $this->assertTrue($response['success']);
        $this->assertNotEmpty($response['src']);
        $this->assertTrue($oItem->images()->count() !== 0);

        // удаление всех, потому что тестирование
        $this->controller()->beforeDeleteForImages($oItem);

        $this->assertTrue($oItem->images()->count() === 0);
    }

    /**
     * @see ArticleController::imageMain()
     *
     * @throws \Throwable
     */
    public function testImageMain()
    {
        $oItem = $this->factoryAdmin();

        // вход под админом чтобы отсделить изменение его аватарки
        $this->actingAs($oItem);

        // передаются изображения директорией
        $response = $this->controller()->imageUpload($this->request([
            'path-images' => [
                public_path('img/default/user.png'),
            ],
        ]), $oItem->id);
        $this->assertTrue($response['success']);
        $this->assertNotEmpty($response['src']);
        $this->assertTrue($oItem->images()->count() !== 0);

        $oImage = $oItem->images()->first();

        // ставим изображению is_main = 1 через контроллер
        $this->controller()->imageMain($this->request(), $oItem->id, $oImage->id);

        $oImage = $oItem->images()->first();
        $this->assertEquals(1, $oImage->is_main);

        // удаление всех, потому что тестирование
        $this->controller()->beforeDeleteForImages($oItem);

        $this->assertTrue($oItem->images()->count() === 0);
    }

    /**
     * @see ArticleController::imageDestroy()
     *
     * @throws \Throwable
     */
    public function testImageDestroy()
    {
        $oItem = $this->factoryAdmin();

        // вход под админом чтобы отсделить изменение его аватарки
        $this->actingAs($oItem);

        // передаются изображения директорией
        $response = $this->controller()->imageUpload($this->request([
            'path-images' => [
                public_path('img/default/user.png'),
            ],
        ]), $oItem->id);
        $this->assertTrue($response['success']);
        $this->assertNotEmpty($response['src']);
        $this->assertTrue($oItem->images()->count() !== 0);

        $oImage = $oItem->images()->first();

        // ставим изображению is_main = 1 через контроллер
        $this->controller()->imageDestroy($this->request(), $oItem->id, $oImage->id);

        $this->assertTrue($oItem->images()->count() === 0);

        // удаление всех, потому что тестирование
        $this->controller()->beforeDeleteForImages($oItem);

        $this->assertTrue($oItem->images()->count() === 0);
    }

    /**
     * @see MainController::paginate()
     */
    public function testQuerySearchBy()
    {
        $oItem = $this->factoryUser();
        $oController = $this->controller();
        $response = $oController->paginate(User::class, 'search', [
            'name' => [
                'translation' => 'like',
                'value' => $oItem->name,
            ]
        ]);
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $response);

        $oItem = $this->factoryUser();
        $oController = $this->controller();
        $response = $oController->paginate(User::class, 'search', [
            'name' => [
                'support' => 'id',
                'translation' => 'like',
                'value' => $oItem->name,
            ]
        ]);
        $this->assertInstanceOf(\Illuminate\Pagination\LengthAwarePaginator::class, $response);
    }
}
