const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const FILES = {
    app: {
        css: {
            from: 'resources/sass/app/app/app.scss',
            to: 'public/css/app.css',
        },
        js: {
            from: 'resources/js/app/app.js',
            to: 'public/js',
        }
    },
    cmf: {
        css: {
            from: 'resources/sass/cmf/app/app.scss',
            to: 'public/css/cmf.css',
        },
        js: {
            from: 'resources/js/cmf/app.js',
            to: 'public/js/cmf',
        }
    }
};

const MIX = {

    mix: null,

    config: {
        css: {
            from: '',
            to: '',
        },
        js: {
            from: '',
            to: '',
        },
    },

    run(mix) {
        let self = this;
        self.mix = mix;
        return self;
    },
    app() {
        let self = this;
        self.config.css = FILES.app.css;
        self.config.js = FILES.app.js;

        self.mix.js(self.config.js.from, self.config.js.to);
        return self;
    },
    cmf() {
        let self = this;
        self.config.css = FILES.cmf.css;
        self.config.js = FILES.cmf.js;

        //self.mix.setResourceRoot('public/admin/resources');
        //self.mix.setPublicPath('public/admin');

        self.mix.js(self.config.js.from, self.config.js.to);
        self.mix.autoload({
            jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
            tether: ['window.Tether', 'Tether'],
            'tether-shepherd': ['Shepherd', 'tether-shepherd'],
            'Popper.js': ['popper.js', 'default'],
            Util: "exports-loader?Util!bootstrap/js/dist/util",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
            toastr: ['window.Toastr', 'toastr'],
            moment: ['window.Moment', 'moment'],
            readmore: 'readmore-js'
        }).webpackConfig({
            resolve: {
                alias: {
                    'load-image': 'blueimp-load-image/js/load-image.js',
                    'load-image-meta': 'blueimp-load-image/js/load-image-meta.js',
                    'load-image-exif': 'blueimp-load-image/js/load-image-exif.js',
                    'canvas-to-blob': 'blueimp-canvas-to-blob/js/canvas-to-blob.js',
                    'jquery-ui/ui/widget': 'blueimp-file-upload/js/vendor/jquery.ui.widget.js'
                }
            }
        }).extract([
            'jquery',
            'toastr',
            'bootstrap',
            'moment',
            'popper.js',
            'tether',
            'tether-shepherd',
            'spin.js',
            'readmore-js',
            'blueimp-file-upload'
        ]);
        return self;
    },
    development(watchUrl) {
        let self = this;
        self.mix.webpackConfig({
            devtool: "inline-source-map"
        });
        self.mix.sass(self.config.css.from, self.config.css.to).options({
            autoprefixer: {
                options: {
                    browsers: [
                        'last 6 versions',
                    ]
                }
            },
            postCss: [
                require('postcss-font-magician'),
                require('css-mqpacker'),
                require('postcss-remove-root'),
            ],
            processCssUrls: true,
            //extractVueStyles: 'css/vue.css',
        }).sourceMaps();
        self.mix.browserSync(watchUrl);
        self.mix.disableNotifications();
    },
    production() {
        let self = this;
        self.mix.options({
            terser: {
                terserOptions: {
                    compress: {
                        drop_console: true
                    }
                }
            }
        });
        self.mix.sass(self.config.css.from, self.config.css.to).options({
            autoprefixer: {
                options: {
                    browsers: [
                        'last 6 versions',
                    ]
                }
            },
            postCss: [
                require('cssnano'),
                require('postcss-font-magician'),
                require('css-mqpacker'),
                require('postcss-remove-root'),
            ],
            processCssUrls: true,
            clearConsole: true,
        }).version();
    }
};

if (process.env.section && process.env.section === 'cmf') {
    if (!mix.inProduction()) {
        MIX.run(mix).cmf().development(process.env.CMF_URL);
    } else {
        MIX.run(mix).cmf().production();
    }
} else {
    if (!mix.inProduction()) {
        MIX.run(mix).app().development(process.env.APP_URL);
    } else {
        MIX.run(mix).app().production();
    }
}
